/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "constants.h"

const gchar *
app_state_to_str (AppState state)
{
  switch (state)
    {
    case APP_STATE_INITIAL:
      return "initial";
    case APP_STATE_PURCHASED:
      return "purchased";
    case APP_STATE_DOWNLOAD_READY:
      return "download-ready";
    case APP_STATE_DOWNLOADING:
      return "downloading";
    case APP_STATE_INSTALLING:
      return "installing";
    case APP_STATE_INSTALLED:
      return "installed";
    case APP_STATE_UPDATE:
      return "update";
    case APP_STATE_UNINSTALLED:
      return "uninstalled";
    default:
      break;
    }

  return "<invalid>";
}

const gchar *
app_status_to_str (AppStatus status)
{
  switch (status)
    {
    case APP_STATUS_INSTALLATION_SUCCESS:
      return "installation-success";
    case APP_STATUS_DOWNLOAD_STARTED:
      return "download-started";
    case APP_STATUS_DOWNLOADING:
      return "downloading";
    case APP_STATUS_DOWNLOAD_COMPLETE:
      return "download-complete";
    case APP_STATUS_DOWNLOADING_FATAL_ERROR:
      return "downloading-fatal-error";
    case APP_STATUS_EXTRACTION_IN_PROGRESS:
      return "extraction-in-progress";
    case APP_STATUS_EXTRACTION_FAILED:
      return "extraction-failed";
    case APP_STATUS_RUNNING_INSTALL_SCRIPT:
      return "running-install-script";
    case APP_STATUS_UNINSTALLATION_FAILED:
      return "uninstallation-failed";
    case APP_STATUS_CREATE_TEMP_SUBVOL:
      return "create-temp-subvol";
    case APP_STATUS_RENAME_TEMP_SUBVOL:
      return "rename-temp-subvol";
    case APP_STATUS_MOUNT_SUBVOL:
      return "mount-subvol";
    case APP_STATUS_UNMOUNT_SUBVOL:
      return "unmount-subvol";
    case APP_STATUS_UPGRADE_SUBVOL:
      return "upgrade-subvol";
    case APP_STATUS_UNINSTALL_WHILE_UPDATING:
      return "uninstall-while-updating";
    default:
      break;
    }

  return "<invalid>";
}

const gchar *
process_type_to_str (ProcessType type)
{
  switch (type)
    {
    case PROCESS_TYPE_APP_INSTALL:
      return "app-install";
    case PROCESS_TYPE_APP_REINSTALL:
      return "app-reinstall";
    case PROCESS_TYPE_APP_UNINSTALL:
      return "app-uninstall";
    case PROCESS_TYPE_APP_UPDATE:
      return "app-update";
    case PROCESS_TYPE_APP_ROLLBACK:
      return "app-rollback";
    case PROCESS_TYPE_APP_USB_INSTALLATION:
      return "app-usb-installation";
    case PROCESS_TYPE_APP_INTERNAL_INSTALLATION:
      return "app-internal-installation";
    case PROCESS_TYPE_APP_REMOVAL:
      return "app-removal";
    default:
      break;
    }

  return "<invalid>";
}

const gchar *
frome_appstore_download_status_result_to_str (FromeAppstoreDownloadStatusResult result)
{
  switch (result)
    {
    case FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_NO_ERROR:
      return "no-error";
    case FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_EXTRACTION_FATAL_ERROR:
      return "error-extraction-fatal-error";
    case FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_DOWNLOADING_APP:
      return "error-downloading-app";
    case FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_UPDATE_VERSION:
      return "error-update-version";
    case FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_DOWNLOAD_ABORTED_BY_USER:
      return "error-aborted-by-user";
    case FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_UNINSTALL_FAILED:
      return "error-uninstall-failed";
    default:
    case FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_REINSTALL_FAILED:
      return "error-reinstall-failed";
      break;
    }

  return "<invalid>";
}
