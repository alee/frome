/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "errors.h"

#include <gio/gio.h>

#include "constants.h"

static const GDBusErrorEntry account_manager_entries[] =
    {
      { FROME_ACCOUNT_MANAGER_ERROR_INVALID_ARGS, "Frome.AccountMgr.Error.InvalidArguments" },
      { FROME_ACCOUNT_MANAGER_ERROR_ACCOUNT_NOT_FOUND, "Frome.AccountMgr.Error.AccountNotFound" },
      { FROME_ACCOUNT_MANAGER_ERROR_MISSING_DETAILS, "Frome.AccountMgr.Error.MissingDetails" },
      { FROME_ACCOUNT_MANAGER_ERROR_CANNOT_OPEN_DB, "Frome.AccountMgr.Error.CannotOpenDatabase" },
      { FROME_ACCOUNT_MANAGER_ERROR_DB_ERROR, "Frome.AccountMgr.Error.DatabaseError" },
      { FROME_ACCOUNT_MANAGER_ERROR_SETTING_DEFAULT_FAILED, "Frome.AccountMgr.Error.SettingDefaultFailed" },
      { FROME_ACCOUNT_MANAGER_ERROR_INVALID_ACCOUNT_TYPE, "Frome.AccountMgr.Error.InvalidAccountType" },
      { FROME_ACCOUNT_MANAGER_ERROR_SERVER_CONNECTION_FAILED, "Frome.AccountMgr.Error.ServerConnectionFailed" },
      { FROME_ACCOUNT_MANAGER_ERROR_DEVICE_ID_NOT_FOUND, "Frome.AccountMgr.Error.DeviceIdNotFound" },
      { FROME_ACCOUNT_MANAGER_ERROR_TARGET_ID_NOT_FOUND, "Frome.AccountMgr.Error.TargetIdNotFound" },
    };

G_STATIC_ASSERT (G_N_ELEMENTS (account_manager_entries) == FROME_ACCOUNT_MANAGER_N_ERRORS);

GQuark
frome_account_manager_error_quark (void)
{
  static volatile gsize id = 0;

  g_dbus_error_register_error_domain ("frome-account-manager-error-quark",
                                      &id, account_manager_entries,
                                      G_N_ELEMENTS (account_manager_entries));

  return (GQuark) id;
}

static const GDBusErrorEntry download_manager_entries[] =
    {
      { FROME_DOWNLOAD_MANAGER_ERROR_INVALID_DOWNLOAD_LINK, "Frome.DnldMgr.Error.InvalidDnldLink" },
      { FROME_DOWNLOAD_MANAGER_ERROR_NO_ACTIVE_ACCOUNT, "Frome.DnldMgr.Error.NoActiveAccount" },
      { FROME_DOWNLOAD_MANAGER_ERROR_PENDING, "Frome.DnldMgr.Error.Pending" },
      { FROME_DOWNLOAD_MANAGER_ERROR_DB, "Frome.DnldMgr.Error.DataBase" },
      { FROME_DOWNLOAD_MANAGER_ERROR_INVALID_ARGS, "Frome.DnldMgr.Error.InvalidArg" }, /* name frome the old Frome */
    };

G_STATIC_ASSERT (G_N_ELEMENTS (download_manager_entries) == FROME_DOWNLOAD_MANAGER_N_ERRORS);

GQuark
frome_download_manager_error_quark (void)
{
  static volatile gsize id = 0;

  g_dbus_error_register_error_domain ("frome-download-manager-error-quark",
                                      &id, download_manager_entries,
                                      G_N_ELEMENTS (download_manager_entries));

  return (GQuark) id;
}
