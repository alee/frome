/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "service.h"

int
main (int argc,
      char *argv[])
{
  g_autoptr (FromeService) service = NULL;
  g_autoptr (GError) error = NULL;
  GOptionContext *context;
  const GOptionEntry entries[] =
      {
        {
            NULL,
        },
      };

  context = g_option_context_new ("- Frome Service");
  g_option_context_add_main_entries (context, entries, NULL);
  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_print ("Option parsing failed: %s\n", error->message);
      return -1;
    }

  service = frome_service_new ();

  frome_service_run (service, &error);

  if (error != NULL)
    {
      g_printerr ("%s: %s\n", argv[0], error->message);
      return error->code;
    }

  return 0;
}
