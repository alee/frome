/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FROME_ACCOUNT_MANAGER_H__
#define __FROME_ACCOUNT_MANAGER_H__

#include <gio/gio.h>

#include "account-db.h"
#include "web-store-client.h"

G_BEGIN_DECLS

#define FROME_TYPE_ACCOUNT_MANAGER (frome_account_manager_get_type ())
G_DECLARE_FINAL_TYPE (FromeAccountManager, frome_account_manager, FROME, ACCOUNT_MANAGER, GObject)

FromeAccountManager *frome_account_manager_new (GDBusConnection *conn,
                                                FromeWebStoreClient *web_store_client,
                                                FromeAccountDB *db,
                                                const gchar *device_id);

G_END_DECLS

#endif /* __FROME_ACCOUNT_MANAGER_H__ */
