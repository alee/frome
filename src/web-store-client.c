/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "web-store-client.h"

#include <libsoup/soup.h>

#include "constants.h"
#include "xml-parser.h"

#define WEB_KEY_FUNCTION "function"
#define WEB_KEY_DEVICE_NUMBER_TARGET "devicenumber_target"
#define WEB_KEY_FIRST_NAME "firstname"
#define WEB_KEY_LAST_NAME "lastname"
#define WEB_KEY_EMAIL "e_mail"
#define WEB_KEY_ORDER_NEWSLETTER "order_newsletter"
#define WEB_KEY_PHONE "telephone"
#define WEB_KEY_STREET "streetname_housenumber"
#define WEB_KEY_CITY "city"
#define WEB_KEY_POSTAL_CODE "postalcode"
#define WEB_KEY_STATE "state"
#define WEB_KEY_COUNTRY "country"
#define WEB_KEY_PASSWORD "password"
#define WEB_KEY_CUSTOMER_ID "customer_id"
#define WEB_KEY_TARGET_ID "target_id"
#define WEB_KEY_APP_STATE "app_state"

#define WEB_API_REGISTER_DEVICE "asales.registerDevice"
#define WEB_API_SET_TARGET_ID "asales.setTargetId"
#define WEB_API_SET_TARGET_APP_STATE "asales.setTargetAppState"
#define WEB_API_SET_STATUS "asales.setStatus"
#define WEB_KEY_PRODUCT_ID "product_id"
#define WEB_KEY_STATUS "status"

struct _FromeWebStoreClient
{
  GObject parent;

  SoupSession *session;
  SoupLogger *logger;

  gchar *server_url;
};

typedef enum {
  PROP_SERVER_URL = 1,
  /*< private >*/
  PROP_LAST = PROP_SERVER_URL
} FromeWebStoreClientProperty;

static GParamSpec *properties[PROP_LAST + 1];

G_DEFINE_TYPE (FromeWebStoreClient, frome_web_store_client, G_TYPE_OBJECT)

static void
frome_web_store_client_get_property (GObject *object,
                                     guint prop_id,
                                     GValue *value,
                                     GParamSpec *pspec)
{
  FromeWebStoreClient *self = FROME_WEB_STORE_CLIENT (object);

  switch ((FromeWebStoreClientProperty) prop_id)
    {
    case PROP_SERVER_URL:
      g_value_set_string (value, self->server_url);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_web_store_client_set_property (GObject *object,
                                     guint prop_id,
                                     const GValue *value,
                                     GParamSpec *pspec)
{
  FromeWebStoreClient *self = FROME_WEB_STORE_CLIENT (object);

  switch ((FromeWebStoreClientProperty) prop_id)
    {
    case PROP_SERVER_URL:
      g_clear_pointer (&self->server_url, g_free);
      self->server_url = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_web_store_client_dispose (GObject *object)
{
  FromeWebStoreClient *self = (FromeWebStoreClient *) object;

  g_clear_object (&self->session);
  g_clear_object (&self->logger);
  g_clear_pointer (&self->server_url, g_free);

  G_OBJECT_CLASS (frome_web_store_client_parent_class)
      ->dispose (object);
}

static void
frome_web_store_client_class_init (FromeWebStoreClientClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = frome_web_store_client_get_property;
  object_class->set_property = frome_web_store_client_set_property;
  object_class->dispose = frome_web_store_client_dispose;

  /**
   * FromeWebStoreClient:server-url:
   *
   * The URL of the web store server.
   */
  properties[PROP_SERVER_URL] = g_param_spec_string (
      "server-url", "Server URL", "Server URL", DEFAULT_WEB_SERVER_URL,
      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
http_logger (SoupLogger *logger,
             SoupLoggerLogLevel level,
             char direction,
             const char *data,
             gpointer user_data)
{
  g_debug ("%c %s", direction, data);
}

static void
frome_web_store_client_init (FromeWebStoreClient *self)
{
  self->server_url = g_strdup (DEFAULT_WEB_SERVER_URL);

  self->session = soup_session_new ();

  if (g_getenv ("FROME_LOG_HTTP") != NULL)
    {
      self->logger = soup_logger_new (SOUP_LOGGER_LOG_BODY, -1);
      soup_logger_set_printer (self->logger, http_logger, self, NULL);
      soup_session_add_feature (self->session, SOUP_SESSION_FEATURE (self->logger));
    }
}

FromeWebStoreClient *
frome_web_store_client_new (void)
{
  return g_object_new (FROME_TYPE_WEB_STORE_CLIENT, NULL);
}

static void
register_device_reply_cb (SoupSession *session,
                          SoupMessage *msg,
                          gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autofree gchar *customer_id = NULL;
  g_autoptr (GError) error = NULL;

  if (msg->status_code != 201)
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_FAILED, "Wrong reply code (%d): %s",
                               msg->status_code, msg->reason_phrase);
      return;
    }

  /* Parse reply to extract the customer id */
  if (!frome_xml_parser_parse_customer (msg->response_body->data,
                                        msg->response_body->length,
                                        &customer_id, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_return_pointer (task, g_steal_pointer (&customer_id), g_free);
}

void
frome_web_store_client_create_account_async (FromeWebStoreClient *self,
                                             const gchar *device,
                                             const gchar *first_name,
                                             const gchar *last_name,
                                             const gchar *email,
                                             const gchar *password,
                                             gboolean order_newsletter,
                                             const gchar *phone,
                                             const gchar *street,
                                             const gchar *city,
                                             const gchar *postal_code,
                                             const gchar *state,
                                             const gchar *country,
                                             GAsyncReadyCallback callback,
                                             gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (SoupMessage) msg = NULL;
  g_autoptr (GHashTable) hash = NULL;

  g_return_if_fail (FROME_IS_WEB_STORE_CLIENT (self));

  task = g_task_new (self, NULL, callback, user_data);

  hash = g_hash_table_new (g_str_hash, g_str_equal);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_FUNCTION, (gpointer) WEB_API_REGISTER_DEVICE);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_DEVICE_NUMBER_TARGET, (gpointer) device);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_FIRST_NAME, (gpointer) first_name);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_LAST_NAME, (gpointer) last_name);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_EMAIL, (gpointer) email);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_PASSWORD, (gpointer) password);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_ORDER_NEWSLETTER, order_newsletter ? (gpointer) "YES" : (gpointer) "NO");
  g_hash_table_insert (hash, (gpointer) WEB_KEY_PHONE, (gpointer) phone);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_STREET, (gpointer) street);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_CITY, (gpointer) city);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_POSTAL_CODE, (gpointer) postal_code);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_STATE, (gpointer) state);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_COUNTRY, (gpointer) country);

  msg = soup_form_request_new_from_hash ("POST", self->server_url, hash);

  soup_session_queue_message (self->session, g_steal_pointer (&msg), register_device_reply_cb,
                              g_steal_pointer (&task));
}

gchar *
frome_web_store_client_create_account_finish (FromeWebStoreClient *self,
                                              GAsyncResult *result,
                                              GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
expect_201_reply_cb (SoupSession *session,
                     SoupMessage *msg,
                     gpointer user_data)
{
  g_autoptr (GTask) task = user_data;

  if (msg->status_code != 201)
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_FAILED, "Wrong reply code (%d): %s",
                               msg->status_code, msg->reason_phrase);
      return;
    }

  g_task_return_boolean (task, TRUE);
}

static const gchar *
account_type_to_str (AccountType type)
{
  switch (type)
    {
    case ACCOUNT_TYPE_DEVEL:
      return "devel";
    case ACCOUNT_TYPE_RELEASE:
      return "release";
    case ACCOUNT_TYPE_BOTH:
      return "both";
    default:
      break;
    }

  g_return_val_if_reached (NULL);
}

void
frome_web_store_client_set_target_id_async (FromeWebStoreClient *self,
                                            const gchar *device,
                                            const gchar *customer_id,
                                            const gchar *target_id,
                                            AccountType account_type,
                                            GAsyncReadyCallback callback,
                                            gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (SoupMessage) msg = NULL;
  g_autoptr (GHashTable) hash = NULL;

  g_return_if_fail (FROME_IS_WEB_STORE_CLIENT (self));

  task = g_task_new (self, NULL, callback, user_data);

  hash = g_hash_table_new (g_str_hash, g_str_equal);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_FUNCTION, (gpointer) WEB_API_SET_TARGET_ID);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_DEVICE_NUMBER_TARGET, (gpointer) device);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_CUSTOMER_ID, (gpointer) customer_id);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_TARGET_ID, (gpointer) target_id);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_APP_STATE, (gpointer) account_type_to_str (account_type));

  msg = soup_form_request_new_from_hash ("POST", self->server_url, hash);

  soup_session_queue_message (self->session, g_steal_pointer (&msg), expect_201_reply_cb,
                              g_steal_pointer (&task));
}

gboolean
frome_web_store_client_set_target_id_finish (FromeWebStoreClient *self,
                                             GAsyncResult *result,
                                             GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

void
frome_web_store_client_set_target_app_state_async (FromeWebStoreClient *self,
                                                   const gchar *device,
                                                   const gchar *customer_id,
                                                   AccountType account_type,
                                                   GAsyncReadyCallback callback,
                                                   gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (SoupMessage) msg = NULL;
  g_autoptr (GHashTable) hash = NULL;

  g_return_if_fail (FROME_IS_WEB_STORE_CLIENT (self));

  task = g_task_new (self, NULL, callback, user_data);

  hash = g_hash_table_new (g_str_hash, g_str_equal);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_FUNCTION, (gpointer) WEB_API_SET_TARGET_APP_STATE);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_DEVICE_NUMBER_TARGET, (gpointer) device);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_CUSTOMER_ID, (gpointer) customer_id);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_APP_STATE, (gpointer) account_type_to_str (account_type));

  msg = soup_form_request_new_from_hash ("POST", self->server_url, hash);

  soup_session_queue_message (self->session, g_steal_pointer (&msg), expect_201_reply_cb,
                              g_steal_pointer (&task));
}

gboolean
frome_web_store_client_set_target_app_state_finish (FromeWebStoreClient *self,
                                                    GAsyncResult *result,
                                                    GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
set_status_reply_cb (SoupSession *session,
                     SoupMessage *msg,
                     gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  AppState status;

  if (msg->status_code != 200)
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_FAILED, "Wrong reply code (%d): %s",
                               msg->status_code, msg->reason_phrase);
      return;
    }

  /* Parse reply to extract the customer id */
  if (!frome_xml_parser_parse_status (msg->response_body->data,
                                      msg->response_body->length,
                                      &status, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_return_int (task, status);
}

static const gchar *
app_state_to_web_str (AppState state)
{
  switch (state)
    {
    case APP_STATE_INITIAL:
      return "1";
    case APP_STATE_PURCHASED:
      return "2";
    case APP_STATE_DOWNLOAD_READY:
      return "3";
    case APP_STATE_DOWNLOADING:
      return "4";
    case APP_STATE_INSTALLING:
      return "5";
    case APP_STATE_INSTALLED:
      return "6";
    case APP_STATE_UPDATE:
      return "7";
    case APP_STATE_UNINSTALLED:
      return "8";
    default:
      break;
    }

  g_return_val_if_reached (NULL);
}

void
frome_web_store_client_set_status_async (FromeWebStoreClient *self,
                                         const gchar *device,
                                         const gchar *customer_id,
                                         const gchar *product_id,
                                         AppState status,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (SoupMessage) msg = NULL;
  g_autoptr (GHashTable) hash = NULL;

  g_return_if_fail (FROME_IS_WEB_STORE_CLIENT (self));

  task = g_task_new (self, NULL, callback, user_data);

  hash = g_hash_table_new (g_str_hash, g_str_equal);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_FUNCTION, (gpointer) WEB_API_SET_STATUS);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_DEVICE_NUMBER_TARGET, (gpointer) device);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_CUSTOMER_ID, (gpointer) customer_id);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_PRODUCT_ID, (gpointer) product_id);
  g_hash_table_insert (hash, (gpointer) WEB_KEY_STATUS, (gpointer) app_state_to_web_str (status));

  msg = soup_form_request_new_from_hash ("POST", self->server_url, hash);

  soup_session_queue_message (self->session, g_steal_pointer (&msg), set_status_reply_cb,
                              g_steal_pointer (&task));
}

AppState
frome_web_store_client_set_status_finish (FromeWebStoreClient *self,
                                          GAsyncResult *result,
                                          GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_int (G_TASK (result), error);
}
