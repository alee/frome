/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FROME_NEWPORT_CLIENT_H__
#define __FROME_NEWPORT_CLIENT_H__

#include <gio/gio.h>

#include <newport.h>

G_BEGIN_DECLS

#define FROME_TYPE_NEWPORT_CLIENT (frome_newport_client_get_type ())
G_DECLARE_FINAL_TYPE (FromeNewportClient, frome_newport_client, FROME, NEWPORT_CLIENT, GObject)

FromeNewportClient *frome_newport_client_new (GDBusConnection *conn);

void frome_newport_client_start_download_async (FromeNewportClient *self,
                                                const gchar *url,
                                                const gchar *path,
                                                GCancellable *cancellable,
                                                GAsyncReadyCallback callback,
                                                gpointer user_data);

NewportDownload *frome_newport_client_start_download_finish (FromeNewportClient *self,
                                                             GAsyncResult *result,
                                                             GError **error);

G_END_DECLS

#endif /* __FROME_NEWPORT_CLIENT_H__ */
