/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "ribchester-client.h"

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <gio/gunixfdlist.h>
#include <ribchester/ribchester.h>

#define RIBCHESTER_BUS_NAME "org.apertis.Ribchester"
#define RIBCHESTER_PATH "/org/apertis/Ribchester/AppStore"

struct _FromeRibchesterClient
{
  GObject parent;

  GDBusConnection *conn; /* owned */

  gboolean initialized;
  GList /* <owned GTask> */ *init_tasks; /* owned */

  RibchesterAppStore *proxy;
};

typedef enum {
  PROP_CONNECTION = 1,
  /*< private >*/
  PROP_LAST = PROP_CONNECTION
} FromeRibchesterClientProperty;

static GParamSpec *properties[PROP_LAST + 1];

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data);

G_DEFINE_TYPE_WITH_CODE (FromeRibchesterClient, frome_ribchester_client, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init));

static void
frome_ribchester_client_get_property (GObject *object,
                                      guint prop_id,
                                      GValue *value,
                                      GParamSpec *pspec)
{
  FromeRibchesterClient *self = FROME_RIBCHESTER_CLIENT (object);

  switch ((FromeRibchesterClientProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->conn);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_ribchester_client_set_property (GObject *object,
                                      guint prop_id,
                                      const GValue *value,
                                      GParamSpec *pspec)
{
  FromeRibchesterClient *self = FROME_RIBCHESTER_CLIENT (object);

  switch ((FromeRibchesterClientProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_assert (self->conn == NULL); /* construct only */
      self->conn = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_ribchester_client_dispose (GObject *object)
{
  FromeRibchesterClient *self = (FromeRibchesterClient *) object;

  g_clear_object (&self->conn);

  g_clear_object (&self->proxy);

  G_OBJECT_CLASS (frome_ribchester_client_parent_class)
      ->dispose (object);
}

static void
frome_ribchester_client_class_init (FromeRibchesterClientClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = frome_ribchester_client_get_property;
  object_class->set_property = frome_ribchester_client_set_property;
  object_class->dispose = frome_ribchester_client_dispose;

  properties[PROP_CONNECTION] = g_param_spec_object (
      "connection", "Connection", "GDBusConnection", G_TYPE_DBUS_CONNECTION,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
frome_ribchester_client_init (FromeRibchesterClient *self)
{
}

FromeRibchesterClient *
frome_ribchester_client_new (GDBusConnection *conn)
{
  return g_object_new (FROME_TYPE_RIBCHESTER_CLIENT,
                       "connection", conn,
                       NULL);
}

static void
fail_init_tasks (FromeRibchesterClient *self,
                 GError *error)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_error (task, g_error_copy (error));
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
complete_init_tasks (FromeRibchesterClient *self)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_boolean (task, TRUE);
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
ribchester_proxy_cb (GObject *source,
                     GAsyncResult *result,
                     gpointer user_data)
{
  FromeRibchesterClient *self = user_data;
  g_autoptr (GError) error = NULL;

  self->proxy = ribchester_app_store_proxy_new_finish (result, &error);
  if (self->proxy == NULL)
    {
      fail_init_tasks (self, error);
      return;
    }

  self->initialized = TRUE;
  complete_init_tasks (self);
}

static void
frome_download_manager_init_async (GAsyncInitable *initable,
                                   int io_priority,
                                   GCancellable *cancellable,
                                   GAsyncReadyCallback callback,
                                   gpointer user_data)
{
  FromeRibchesterClient *self = FROME_RIBCHESTER_CLIENT (initable);
  g_autoptr (GTask) task = NULL;
  gboolean start_init;

  g_return_if_fail (self->conn != NULL);

  task = g_task_new (initable, cancellable, callback, user_data);

  if (self->initialized)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  start_init = (self->init_tasks == NULL);
  self->init_tasks = g_list_append (self->init_tasks, g_object_ref (task));

  if (start_init)
    ribchester_app_store_proxy_new (self->conn, G_DBUS_PROXY_FLAGS_NONE,
                                    RIBCHESTER_BUS_NAME, RIBCHESTER_PATH, cancellable,
                                    ribchester_proxy_cb, self);
}

static gboolean
frome_download_manager_init_finish (GAsyncInitable *initable,
                                    GAsyncResult *result,
                                    GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, initable), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data)
{
  GAsyncInitableIface *iface = g_iface;

  iface->init_async = frome_download_manager_init_async;
  iface->init_finish = frome_download_manager_init_finish;
}

static const gchar *
transaction_stage_to_str (RibchesterAppStoreTransactionStage stage)
{
  switch (stage)
    {
    case RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLED:
      return "cancelled";
    case RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLING:
      return "cancelling";
    case RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FAILED:
      return "failed";
    case RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FAILING:
      return "failing";
    case RIBCHESTER_APP_STORE_TRANSACTION_STAGE_STARTING:
      return "starting";
    case RIBCHESTER_APP_STORE_TRANSACTION_STAGE_VERIFYING:
      return "verifying";
    case RIBCHESTER_APP_STORE_TRANSACTION_STAGE_PREPARING:
      return "preparing";
    case RIBCHESTER_APP_STORE_TRANSACTION_STAGE_UNPACKING:
      return "unpacking";
    case RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FINISHING:
      return "finishing";
    case RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED:
      return "succeeded";
    default:
      break;
    }

  return "<invalid>";
}

static gboolean
complete_task_if_needed (RibchesterAppStoreTransactionStage stage,
                         GTask *task)
{
  gboolean done = FALSE;

  g_debug ("Ribchester transaction stage: %s", transaction_stage_to_str (stage));

  if (stage == RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED)
    {
      g_task_return_boolean (task, TRUE);
      done = TRUE;
    }
  else if (stage < 0)
    {
      g_task_return_new_error (task, G_IO_ERROR, G_IO_ERROR_FAILED,
                               "wrong transaction stage: %s",
                               transaction_stage_to_str (stage));
      done = TRUE;
    }

  if (done)
    {
      /* call on Release() on the transaction as we're done with it */
      RibchesterAppStoreTransaction *transaction;

      transaction = g_task_get_task_data (task);
      /* No need to wait for the reply */
      ribchester_app_store_transaction_call_release (transaction, NULL, NULL, NULL);
    }

  return done;
}

static void
transaction_stage_cb (GObject *object,
                      GParamSpec *spec,
                      GTask *task)
{
  RibchesterAppStoreTransaction *transaction = (RibchesterAppStoreTransaction *) object;
  RibchesterAppStoreTransactionStage stage;

  stage = ribchester_app_store_transaction_get_stage (transaction);

  if (!complete_task_if_needed (stage, task))
    return;

  g_object_unref (task);
}

static void
transaction_proxy_cb (GObject *source,
                      GAsyncResult *result,
                      gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (RibchesterAppStoreTransaction) transaction = NULL;
  RibchesterAppStoreTransactionStage stage;

  transaction = ribchester_app_store_transaction_proxy_new_finish (result, &error);
  if (transaction == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_set_task_data (task, g_object_ref (transaction), g_object_unref);

  stage = ribchester_app_store_transaction_get_stage (transaction);

  if (complete_task_if_needed (stage, task))
    return;

  g_signal_connect (transaction, "notify::stage",
                    G_CALLBACK (transaction_stage_cb), g_steal_pointer (&task));
}

static void
install_app_bundle_cb (GObject *source,
                       GAsyncResult *result,
                       gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autofree gchar *path = NULL;
  g_autoptr (GError) error = NULL;
  GCancellable *cancellable;

  if (!ribchester_app_store_call_install_app_bundle_finish (RIBCHESTER_APP_STORE (source),
                                                            &path, NULL, result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  cancellable = g_task_get_cancellable (task);

  ribchester_app_store_transaction_proxy_new (g_dbus_proxy_get_connection (G_DBUS_PROXY (source)),
                                              G_DBUS_PROXY_FLAGS_NONE,
                                              g_dbus_proxy_get_name (G_DBUS_PROXY (source)),
                                              path,
                                              cancellable,
                                              transaction_proxy_cb,
                                              g_steal_pointer (&task));
}

void
frome_ribchester_client_install_async (FromeRibchesterClient *self,
                                       const gchar *path,
                                       GCancellable *cancellable,
                                       GAsyncReadyCallback callback,
                                       gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  gint handle;
  g_autoptr (GUnixFDList) fd_list = NULL;
  int fd;

  g_return_if_fail (path != NULL);

  task = g_task_new (self, cancellable, callback, user_data);

  fd = open (path, O_RDONLY);
  if (fd == -1)
    {
      g_task_return_new_error (task, G_IO_ERROR, g_io_error_from_errno (errno),
                               "Failed to open %s", path);
      return;
    }

  fd_list = g_unix_fd_list_new ();
  handle = g_unix_fd_list_append (fd_list, fd, NULL);

  ribchester_app_store_call_install_app_bundle (self->proxy,
                                                g_variant_new_handle (handle),
                                                fd_list,
                                                cancellable,
                                                install_app_bundle_cb,
                                                g_steal_pointer (&task));

  close (fd);
}

gboolean
frome_ribchester_client_install_finish (FromeRibchesterClient *self,
                                        GAsyncResult *result,
                                        GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
uninstall_app_cb (GObject *source,
                  GAsyncResult *result,
                  gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  gboolean status;
  guint err_info;

  if (!ribchester_app_store_call_uninstall_app_finish (RIBCHESTER_APP_STORE (source),
                                                       &status, &err_info, result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  if (!status)
    {
      /* Why is Ribchester not returning an error? */
      g_task_return_new_error (task, G_DBUS_ERROR, G_DBUS_ERROR_FAILED,
                               "Wrong status returned by Ribchester (error code: %u)",
                               err_info);
      return;
    }

  g_task_return_boolean (task, TRUE);
}

void
frome_ribchester_client_uninstall_async (FromeRibchesterClient *self,
                                         const gchar *bundle_id,
                                         GCancellable *cancellable,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data)
{
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);

  ribchester_app_store_call_uninstall_app (self->proxy,
                                           bundle_id,
                                           cancellable,
                                           uninstall_app_cb,
                                           g_steal_pointer (&task));
}

gboolean
frome_ribchester_client_uninstall_finish (FromeRibchesterClient *self,
                                          GAsyncResult *result,
                                          GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
reinstall_app_cb (GObject *source,
                  GAsyncResult *result,
                  gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  gboolean status;
  guint err_info;

  if (!ribchester_app_store_call_re_install_app_finish (RIBCHESTER_APP_STORE (source),
                                                        &status, &err_info, result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  if (!status)
    {
      /* Why is Ribchester not returning an error? */
      g_task_return_new_error (task, G_DBUS_ERROR, G_DBUS_ERROR_FAILED,
                               "Wrong status returned by Ribchester (error code: %u)",
                               err_info);
      return;
    }

  g_task_return_boolean (task, TRUE);
}

void
frome_ribchester_client_reinstall_async (FromeRibchesterClient *self,
                                         const gchar *bundle_id,
                                         GCancellable *cancellable,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data)
{
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);

  ribchester_app_store_call_re_install_app (self->proxy,
                                            bundle_id,
                                            cancellable,
                                            reinstall_app_cb,
                                            g_steal_pointer (&task));
}

gboolean
frome_ribchester_client_reinstall_finish (FromeRibchesterClient *self,
                                          GAsyncResult *result,
                                          GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
rollback_cb (GObject *source,
             GAsyncResult *result,
             gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *version = NULL;

  if (!ribchester_app_store_call_roll_back_finish (RIBCHESTER_APP_STORE (source),
                                                   &version, result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_return_pointer (task, g_steal_pointer (&version), g_free);
}

void
frome_ribchester_client_rollback_async (FromeRibchesterClient *self,
                                        const gchar *bundle_id,
                                        GCancellable *cancellable,
                                        GAsyncReadyCallback callback,
                                        gpointer user_data)
{
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);

  ribchester_app_store_call_roll_back (self->proxy,
                                       bundle_id,
                                       cancellable,
                                       rollback_cb,
                                       g_steal_pointer (&task));
}

gchar *
frome_ribchester_client_rollback_finish (FromeRibchesterClient *self,
                                         GAsyncResult *result,
                                         GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_pointer (G_TASK (result), error);
}
