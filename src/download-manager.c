/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "download-manager.h"

#include <glib/gstdio.h>

#include "constants.h"
#include "errors.h"
#include "newport-client.h"
#include "ribchester-client.h"

#include "dbus/Frome.DnldMgr.h"

/* Interaction between the web store, Frome and Ribchester:
 *
 * - The 'product_id' passed to Frome's D-Bus methods is the ID used by the web
 *   store to identify products. It's only used by Frome and the web store.
 *   It's also used as the primary key in the application DB.
 * - The 'app_name' passed to Frome's D-Bus methods is the bundle ID of the
 *   bundle storing the application. That's the ID used by Ribchester to
 *   manage bundles.
 */

struct _FromeDownloadManager
{
  GObject parent;

  GDBusConnection *conn;     /* owned */
  GDBusConnection *sys_conn; /* owned */
  gchar *device_id;

  GCancellable *cancellable;
  gboolean registered;
  guint own_name_id;                     /* 0 if the server is not registered yet */
  GList /* <owned GTask> */ *init_tasks; /* owned */
  GError *invalidated_error;             /* owned, NULL until the service is invalidated */

  FromeDnldMgr *service;
  FromeWebStoreClient *web;
  FromeAccountDB *account_db;
  FromeAppDB *app_db;

  FromeNewportClient *newport;                                                                    /* owned */
  GHashTable /* <borrowed const gchar *> product-id -> <owned DownloadOperation *> */ *downloads; /* owned */

  FromeRibchesterClient *ribchester;
};

typedef enum {
  PROP_CONNECTION = 1,
  PROP_SYS_CONNECTION,
  PROP_WEB_STORE_CLIENT,
  PROP_ACCOUNT_DB,
  PROP_DEVICE_ID,
  PROP_APP_DB,
  /*< private >*/
  PROP_LAST = PROP_APP_DB
} FromeDownloadManagerProperty;

static GParamSpec *properties[PROP_LAST + 1];

enum
{
  SIG_INVALIDATED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data);

G_DEFINE_TYPE_WITH_CODE (FromeDownloadManager, frome_download_manager, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init));

static void
frome_download_manager_get_property (GObject *object,
                                     guint prop_id,
                                     GValue *value,
                                     GParamSpec *pspec)
{
  FromeDownloadManager *self = FROME_DOWNLOAD_MANAGER (object);

  switch ((FromeDownloadManagerProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->conn);
      break;
    case PROP_SYS_CONNECTION:
      g_value_set_object (value, self->sys_conn);
      break;
    case PROP_WEB_STORE_CLIENT:
      g_value_set_object (value, self->web);
      break;
    case PROP_ACCOUNT_DB:
      g_value_set_object (value, self->account_db);
      break;
    case PROP_DEVICE_ID:
      g_value_set_string (value, self->device_id);
      break;
    case PROP_APP_DB:
      g_value_set_object (value, self->app_db);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_download_manager_set_property (GObject *object,
                                     guint prop_id,
                                     const GValue *value,
                                     GParamSpec *pspec)
{
  FromeDownloadManager *self = FROME_DOWNLOAD_MANAGER (object);

  switch ((FromeDownloadManagerProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_assert (self->conn == NULL); /* construct only */
      self->conn = g_value_dup_object (value);
      break;
    case PROP_SYS_CONNECTION:
      g_assert (self->sys_conn == NULL); /* construct only */
      self->sys_conn = g_value_dup_object (value);
      break;
    case PROP_WEB_STORE_CLIENT:
      g_assert (self->web == NULL); /* construct only */
      self->web = g_value_dup_object (value);
      break;
    case PROP_ACCOUNT_DB:
      g_assert (self->account_db == NULL); /* construct only */
      self->account_db = g_value_dup_object (value);
      break;
    case PROP_DEVICE_ID:
      g_assert (self->device_id == NULL); /* construct only */
      self->device_id = g_value_dup_string (value);
      break;
    case PROP_APP_DB:
      g_assert (self->app_db == NULL); /* construct only */
      self->app_db = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_download_manager_dispose (GObject *object)
{
  FromeDownloadManager *self = (FromeDownloadManager *) object;

  g_assert (self->init_tasks == NULL); /* Init tasks keep a ref on self */

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  if (self->own_name_id != 0)
    {
      g_bus_unown_name (self->own_name_id);
      self->own_name_id = 0;
    }

  g_clear_object (&self->service);
  g_clear_object (&self->web);
  g_clear_object (&self->conn);
  g_clear_object (&self->sys_conn);
  g_clear_object (&self->account_db);
  g_clear_object (&self->app_db);

  g_clear_object (&self->newport);
  g_clear_object (&self->ribchester);

  g_clear_error (&self->invalidated_error);

  if (self->downloads != NULL)
    {
      /* DownloadOperation keeps a ref on self */
      g_assert_cmpuint (g_hash_table_size (self->downloads), ==, 0);
      g_clear_pointer (&self->downloads, g_hash_table_unref);
    }

  G_OBJECT_CLASS (frome_download_manager_parent_class)
      ->dispose (object);
}

static void
frome_download_manager_class_init (FromeDownloadManagerClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = frome_download_manager_get_property;
  object_class->set_property = frome_download_manager_set_property;
  object_class->dispose = frome_download_manager_dispose;

  /**
   * FromeDownloadManager:connection:
   *
   * The #GDBusConnection used by the manager to communicate on the bus.
   */
  properties[PROP_CONNECTION] = g_param_spec_object (
      "connection", "Connection", "GDBusConnection", G_TYPE_DBUS_CONNECTION,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * FromeDownloadManager:system-connection:
   *
   * The #GDBusConnection used by the manager to communicate on the system bus.
   */
  properties[PROP_SYS_CONNECTION] = g_param_spec_object (
      "system-connection", "System Connection", "GDBusConnection", G_TYPE_DBUS_CONNECTION,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * FromeDownloadManager:web-store-client:
   *
   * The #FromeWebStoreClient used by the manager to communicate with the
   * web store.
   */
  properties[PROP_WEB_STORE_CLIENT] = g_param_spec_object (
      "web-store-client", "Web Store Client", "FromeWebStoreClient", FROME_TYPE_WEB_STORE_CLIENT,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * FromeDownloadManager:account-db:
   *
   * The #FromeAccountDB used by the manager to store and retrieve accounts information.
   */
  properties[PROP_ACCOUNT_DB] = g_param_spec_object (
      "account-db", "Account DB", "FromeAccountDB", FROME_TYPE_ACCOUNT_DB,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * FromeDownloadManager:device-id:
   *
   * The hash of the device ID on which the manager is running
   */
  properties[PROP_DEVICE_ID] = g_param_spec_string (
      "device-id", "Device ID", "Device ID", NULL,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * FromeDownloadManager:application-db:
   *
   * The #FromeAppDB used by the manager to store and retrieve applications information.
   */
  properties[PROP_APP_DB] = g_param_spec_object (
      "application-db", "Application DB", "FromeAppDB", FROME_TYPE_APP_DB,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * FromeDownloadManager::invalidated:
   * @self: a #FromeDownloadManager
   * @error: error which caused @self to be invalidated
   *
   * Emitted when @self is no longer usable.
   * The most common reason for this signal to be emitted is if the
   * underlying D-Bus service name is lost.
   */
  signals[SIG_INVALIDATED] = g_signal_new ("invalidated", FROME_TYPE_DOWNLOAD_MANAGER,
                                           G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                           G_TYPE_NONE, 1, G_TYPE_ERROR);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static gchar *
build_download_path (const gchar *download_url)
{
  g_auto (GStrv) tmp = NULL;
  guint len;

  tmp = g_strsplit (download_url, "/", 0);
  len = g_strv_length (tmp);

  if (len < 2)
    return NULL;

  return g_build_filename (g_get_user_special_dir (G_USER_DIRECTORY_DOWNLOAD),
                           "frome", tmp[len - 1], NULL);
}

typedef enum {
  DOWNLOAD_OP_STATE_DOWNLOADING,
  DOWNLOAD_OP_STATE_INSTALLING,
  DOWNLOAD_OP_STATE_DONE,
  DOWNLOAD_OP_STATE_FAILED,
  DOWNLOAD_OP_STATE_CANCELLING,
  DOWNLOAD_OP_STATE_CANCELLED,
} DownloadOperationState;

typedef struct
{
  FromeDownloadManager *self;
  gchar *product_id;
  gchar *app_name;
  gchar *app_version;
  gchar *download_url;
  gchar *download_path;
  FromeAccount *account;
  gboolean update;

  NewportDownload *download_proxy;
  FromeAppDBEntry *entry;
  DownloadOperationState state;

  guint ref_count;
} DownloadOperation;

/* @account: (transfer full) */
static DownloadOperation *
download_operation_new (FromeDownloadManager *self,
                        const gchar *product_id,
                        const gchar *app_name,
                        const gchar *app_version,
                        const gchar *download_url,
                        const gchar *download_path,
                        FromeAccount *account,
                        gboolean update)
{
  DownloadOperation *op = g_new0 (DownloadOperation, 1);

  op->ref_count = 1;

  op->self = g_object_ref (self);
  op->product_id = g_strdup (product_id);
  op->app_name = g_strdup (app_name);
  op->app_version = g_strdup (app_version);
  op->download_url = g_strdup (download_url);
  op->download_path = g_strdup (download_path);
  op->account = account;
  op->update = update;
  op->state = DOWNLOAD_OP_STATE_DOWNLOADING;
  return op;
}

static void
download_operation_unref (DownloadOperation *op)
{
  op->ref_count--;

  if (op->ref_count > 0)
    return;

  g_object_unref (op->self);
  g_free (op->product_id);
  g_free (op->app_name);
  g_free (op->app_version);
  g_free (op->download_url);
  g_free (op->download_path);
  frome_account_unref (op->account);

  g_clear_object (&op->download_proxy);
  g_clear_object (&op->entry);
  g_free (op);
}

static DownloadOperation *
download_operation_ref (DownloadOperation *op)
{
  op->ref_count++;

  return op;
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (DownloadOperation, download_operation_unref)

static void
save_entry (FromeAppDBEntry *entry)
{
  g_autoptr (GError) error = NULL;

  if (!frome_app_db_entry_save (entry, &error))
    g_warning ("Failed to save entry: %s", error->message);
}

static void
set_status_cb (GObject *source,
               GAsyncResult *result,
               gpointer user_data)
{
  g_autoptr (FromeAppDBEntry) entry = user_data;
  g_autoptr (GError) error = NULL;
  AppState state;

  state = frome_web_store_client_set_status_finish (FROME_WEB_STORE_CLIENT (source),
                                                    result, &error);

  if (error == NULL)
    {
      frome_app_db_entry_set_store_app_state (entry, state);
      save_entry (entry);
    }
  else
    {
      g_warning ("Failed to send application status: %s", error->message);
    }
}

static void
update_app_state (FromeDownloadManager *self,
                  FromeAppDBEntry *entry,
                  const gchar *product_id,
                  const gchar *customer_id,
                  AppState state)
{
  frome_app_db_entry_set_target_app_state (entry, state);
  save_entry (entry);

  /* Notify web shop server */
  frome_web_store_client_set_status_async (self->web, self->device_id,
                                           customer_id,
                                           product_id, state,
                                           set_status_cb,
                                           g_object_ref (entry));
}

static void
download_operation_update_app_state (DownloadOperation *op,
                                     AppState state)
{
  update_app_state (op->self, op->entry, op->product_id,
                    op->account->customer_id, state);
}

static void
update_db_start_download (DownloadOperation *op)
{
  const gchar *old_version;

  old_version = frome_app_db_entry_get_version (op->entry);

  frome_app_db_entry_set_app_name (op->entry, op->app_name);
  frome_app_db_entry_set_download_url (op->entry, op->download_url);
  /* The old Frome was storing the download path here for some reason */
  frome_app_db_entry_set_install_path (op->entry, op->download_path);
  /* Don't set the app_folder_name as it's handled by Ribchester now. */
  download_operation_update_app_state (op, APP_STATE_DOWNLOADING);
  frome_app_db_entry_set_app_status (op->entry, APP_STATUS_DOWNLOAD_STARTED);
  frome_app_db_entry_set_version (op->entry, op->app_version);
  if (op->update)
    frome_app_db_entry_set_process_type (op->entry, PROCESS_TYPE_APP_UPDATE);
  else
    frome_app_db_entry_set_process_type (op->entry, PROCESS_TYPE_APP_INSTALL);

  if (old_version != NULL)
    frome_app_db_entry_set_rollback_version (op->entry, old_version);
  else
    frome_app_db_entry_set_rollback_version (op->entry, ROLLBACK_NOT_APPLICABLE);

  save_entry (op->entry);
}

static void
emit_appstore_download_progress (DownloadOperation *op,
                                 guint64 current_file_size,
                                 guint64 total_file_size)
{
  gdouble progress;

  if (total_file_size == 0)
    {
      g_warning ("Total file size shouldn't be 0");
      return;
    }

  progress = (gdouble) current_file_size / total_file_size;

  g_debug ("download progress (product %s): %lu / %lu (%f %%)", op->product_id,
           current_file_size, total_file_size, progress * 100);

  g_debug ("emit DownloadProgress loading");
  frome_dnld_mgr_emit_appstore_download_progress (op->self->service, op->app_name,
                                                  op->product_id,
                                                  FROME_APPSTORE_SIG_STATE_LOADING,
                                                  progress);
}

static void
download_progress_cb (NewportDownload *proxy,
                      guint64 current_file_size,
                      guint64 total_file_size,
                      guint64 download_speed,
                      guint64 elapsed_time,
                      guint64 remain_time,
                      DownloadOperation *op)
{
  emit_appstore_download_progress (op, current_file_size, total_file_size);
}

static const gchar *
newport_download_state_to_str (NewportDownloadState state)
{
  switch (state)
    {
    case NEWPORT_DOWNLOAD_STATE_SUCCESS:
      return "success";
    case NEWPORT_DOWNLOAD_STATE_IN_PROGRESS:
      return "in-progress";
    case NEWPORT_DOWNLOAD_STATE_FAILED:
      return "failed";
    case NEWPORT_DOWNLOAD_STATE_PAUSED_BY_USER:
      return "paused-by-user";
    case NEWPORT_DOWNLOAD_STATE_PAUSED_BY_SYSTEM:
      return "paused-by-system";
    case NEWPORT_DOWNLOAD_STATE_CANCELLED:
      return "cancelled";
    case NEWPORT_DOWNLOAD_STATE_QUEUED:
      return "queued";
    default:
      break;
    }

  return "<invalid>";
}

static const gchar *
newport_download_error_to_str (NewportDownloadManagerError error)
{
  switch (error)
    {
    case NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED:
      return "failed";
    case NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK:
      return "network";
    case NEWPORT_DOWNLOAD_MANAGER_ERROR_NO_MEMORY:
      return "no-memory";
    case NEWPORT_DOWNLOAD_MANAGER_ERROR_UNRECOGNIZED_FORMAT:
      return "unrecognized-format";
    case NEWPORT_N_DOWNLOAD_MANAGER_ERRORS:
    default:
      break;
    }

  return "<invalid>";
}

static void
install_cb (GObject *source,
            GAsyncResult *result,
            gpointer user_data)
{
  DownloadOperation *op = user_data;
  g_autoptr (GError) error = NULL;

  if (!frome_ribchester_client_install_finish (FROME_RIBCHESTER_CLIENT (source),
                                               result, &error))
    {
      g_debug ("Installation of product %s failed: %s", op->product_id, error->message);

      op->state = DOWNLOAD_OP_STATE_FAILED;

      frome_app_db_entry_set_app_status (op->entry, APP_STATUS_EXTRACTION_FAILED);

      frome_dnld_mgr_emit_appstore_download_status (op->self->service, op->app_name,
                                                    op->product_id, "", FROME_APPSTORE_DOWNLOAD_STATUS_FAILED,
                                                    FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_EXTRACTION_FATAL_ERROR);
    }
  else
    {
      g_debug ("Installation of product %s completed", op->product_id);

      op->state = DOWNLOAD_OP_STATE_DONE;

      frome_app_db_entry_set_app_status (op->entry, APP_STATUS_INSTALLATION_SUCCESS);
      download_operation_update_app_state (op, APP_STATE_INSTALLED);

      frome_dnld_mgr_emit_appstore_download_status (op->self->service, op->app_name,
                                                    op->product_id, FROME_APPSTORE_SIG_STATE_INSTALLED,
                                                    FROME_APPSTORE_DOWNLOAD_STATUS_SUCCESS,
                                                    FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_NO_ERROR);

      g_unlink (op->download_path);
    }

  save_entry (op->entry);
  g_hash_table_remove (op->self->downloads, op->product_id);
}

static void
download_complete (DownloadOperation *op)
{
  g_debug ("Download of product %s completed", op->product_id);

  op->state = DOWNLOAD_OP_STATE_INSTALLING;

  /* If another download is started to re-download the same URL (for an update
   * for example), Newport will re-use the same Download object. We don't want
   * this proxy to receive the state change signals for this new download so we
   * destroy it as soon as we're done with it. */
  g_clear_object (&op->download_proxy);

  /* Update DB */
  frome_app_db_entry_set_app_status (op->entry, APP_STATUS_EXTRACTION_IN_PROGRESS);
  download_operation_update_app_state (op, APP_STATE_INSTALLING);
  save_entry (op->entry);

  /* FIXME: Old frome was sending 0.001 as progress when starting installing.
   * Do we want to keep doing that? */
  frome_dnld_mgr_emit_appstore_download_progress (op->self->service, op->app_name,
                                                  op->product_id,
                                                  FROME_APPSTORE_SIG_STATE_INSTALLING,
                                                  0.001);

  frome_ribchester_client_install_async (op->self->ribchester, op->download_path,
                                         NULL, install_cb, op);
}

static void
download_failed (DownloadOperation *op)
{
  g_debug ("Download of product %s failed", op->product_id);

  op->state = DOWNLOAD_OP_STATE_FAILED;

  /* See comment in download_complete() explaining why we destroy the proxy */
  g_clear_object (&op->download_proxy);

  frome_app_db_entry_set_app_status (op->entry, APP_STATUS_DOWNLOADING_FATAL_ERROR);
  save_entry (op->entry);

  if (op->update)
    frome_dnld_mgr_emit_appstore_download_status (op->self->service, op->app_name,
                                                  op->product_id, "", FROME_APPSTORE_DOWNLOAD_STATUS_FAILED,
                                                  FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_UPDATE_VERSION);
  else
    frome_dnld_mgr_emit_appstore_download_status (op->self->service, op->app_name,
                                                  op->product_id, "", FROME_APPSTORE_DOWNLOAD_STATUS_FAILED,
                                                  FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_DOWNLOADING_APP);

  g_hash_table_remove (op->self->downloads, op->product_id);
}

static void
download_operation_cancelled (DownloadOperation *op)
{
  g_debug ("Cancelled download of product %s", op->product_id);
  op->state = DOWNLOAD_OP_STATE_CANCELLED;

  frome_dnld_mgr_emit_appstore_download_status (op->self->service,
                                                op->app_name,
                                                op->product_id,
                                                FROME_APPSTORE_SIG_STATE_LOADING,
                                                FROME_APPSTORE_DOWNLOAD_STATUS_CANCELLED,
                                                FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_DOWNLOAD_ABORTED_BY_USER);

  g_hash_table_remove (op->self->downloads, op->product_id);
}

static void
download_state_changed (DownloadOperation *op)
{
  NewportDownloadState state;
  NewportDownloadManagerError error;

  state = newport_download_get_state (op->download_proxy);
  error = newport_download_get_error (op->download_proxy);

  g_debug ("download state changed (product %s): %s %s", op->product_id,
           newport_download_state_to_str (state),
           state == NEWPORT_DOWNLOAD_STATE_FAILED ? newport_download_error_to_str (error) : "");

  if (state == NEWPORT_DOWNLOAD_STATE_SUCCESS)
    {
      download_complete (op);
    }
  else if (state == NEWPORT_DOWNLOAD_STATE_FAILED)
    {
      download_failed (op);
    }
}

static void
download_state_cb (NewportDownload *proxy,
                   GParamSpec *spec,
                   DownloadOperation *op)
{
  download_state_changed (op);
}

static void
cancel_download_cb (GObject *proxy,
                    GAsyncResult *result,
                    gpointer user_data)
{
  g_autoptr (DownloadOperation) op = user_data;
  g_autoptr (GError) error = NULL;

  if (!newport_download_call_cancel_finish (NEWPORT_DOWNLOAD (proxy), result,
                                            &error))
    g_warning ("Failed to cancel download: %s", error->message);

  download_operation_cancelled (op);
}

static void
download_operation_cancel (DownloadOperation *op)
{
  newport_download_call_cancel (op->download_proxy, NULL, cancel_download_cb,
                                download_operation_ref (op));
}

static void
newport_start_download_cb (GObject *source,
                           GAsyncResult *result,
                           gpointer user_data)
{
  DownloadOperation *op = user_data;
  g_autoptr (GError) error = NULL;
  NewportDownloadState state;
  guint64 total_file_size;

  g_assert (op->download_proxy == NULL);
  op->download_proxy = frome_newport_client_start_download_finish (FROME_NEWPORT_CLIENT (source),
                                                                   result, &error);
  if (op->download_proxy == NULL)
    {
      g_warning ("Failed to start download for product %s: %s", op->product_id, error->message);
      download_failed (op);
      return;
    }

  if (op->state == DOWNLOAD_OP_STATE_CANCELLING)
    {
      download_operation_cancel (op);
      return;
    }

  g_signal_connect (op->download_proxy, "download-progress",
                    G_CALLBACK (download_progress_cb), op);
  g_signal_connect (op->download_proxy, "notify::state",
                    G_CALLBACK (download_state_cb), op);

  state = newport_download_get_state (op->download_proxy);

  total_file_size = newport_download_get_total_size (op->download_proxy);

  /* Report initial state using the download progress signal if we are downloading
   * and already have enough enough to compute the percentage progress. */
  if (state == NEWPORT_DOWNLOAD_STATE_IN_PROGRESS &&
      total_file_size != 0)
    {
      emit_appstore_download_progress (op, newport_download_get_downloaded_size (op->download_proxy),
                                       total_file_size);
    }
  else
    {
      download_state_changed (op);
    }
}

static FromeAccount *
get_active_user (FromeDownloadManager *self,
                 GDBusMethodInvocation *invocation)
{
  g_autoptr (FromeAccount) account = NULL;

  account = frome_account_db_get_active_user (self->account_db);
  if (account == NULL)
    {
      g_dbus_method_invocation_return_error_literal (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                                     FROME_DOWNLOAD_MANAGER_ERROR_NO_ACTIVE_ACCOUNT,
                                                     "No active account configured");
      return NULL;
    }

  return g_steal_pointer (&account);
}

static gboolean
start_download (FromeDownloadManager *self,
                GDBusMethodInvocation *invocation,
                const gchar *app_name,
                const gchar *download_url,
                const gchar *product_id,
                const gchar *app_version,
                gint package_size,
                gboolean update)
{
  g_autofree gchar *path = NULL;
  g_autoptr (FromeAccount) account = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (DownloadOperation) op = NULL;

  if (g_hash_table_lookup (self->downloads, product_id) != NULL)
    {
      g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                             FROME_DOWNLOAD_MANAGER_ERROR_PENDING,
                                             "Pending download operation for product %s", product_id);
      return FALSE;
    }

  /* TODO: old Frome was checking if we have enough space to install the app. */

  path = build_download_path (download_url);
  if (path == NULL)
    {
      g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                             FROME_DOWNLOAD_MANAGER_ERROR_INVALID_DOWNLOAD_LINK,
                                             "Invalid URL: %s", download_url);
      return FALSE;
    }

  account = get_active_user (self, invocation);
  if (account == NULL)
    return FALSE;

  op = download_operation_new (self, product_id, app_name, app_version,
                               download_url, path, g_steal_pointer (&account), update);
  op->entry = frome_app_db_ensure_entry (op->self->app_db, op->product_id, &error);

  if (op->entry == NULL)
    {
      g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                             FROME_DOWNLOAD_MANAGER_ERROR_DB,
                                             "%s", error->message);
      return FALSE;
    }

  g_hash_table_insert (self->downloads, op->product_id, download_operation_ref (op));

  /* Start downloading the app */
  g_debug ("Starting download of product %s (%s) to %s", product_id,
           download_url, path);

  frome_newport_client_start_download_async (self->newport, download_url, path,
                                             self->cancellable,
                                             newport_start_download_cb, op);

  update_db_start_download (op);

  return TRUE;
}

static gboolean
on_handle_start_appstore_download (FromeDnldMgr *service,
                                   GDBusMethodInvocation *invocation,
                                   const gchar *app_name,
                                   const gchar *download_url,
                                   const gchar *product_id,
                                   const gchar *app_version,
                                   gint package_size,
                                   FromeDownloadManager *self)
{
  if (!start_download (self, invocation, app_name, download_url,
                       product_id, app_version, package_size, FALSE))
    return TRUE;

  frome_dnld_mgr_complete_start_appstore_download (service, invocation);
  return TRUE;
}

static gboolean
on_handle_cancel_appstore_download (FromeDnldMgr *service,
                                    GDBusMethodInvocation *invocation,
                                    const gchar *app_name,
                                    const gchar *download_url,
                                    const gchar *product_id,
                                    FromeDownloadManager *self)
{
  DownloadOperation *op;

  op = g_hash_table_lookup (self->downloads, product_id);

  if (op == NULL)
    {
      g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                             FROME_DOWNLOAD_MANAGER_ERROR_INVALID_ARGS,
                                             "No download operation for product %s", product_id);
      return TRUE;
    }

  if (g_strcmp0 (op->app_name, app_name) != 0)
    {
      g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                             FROME_DOWNLOAD_MANAGER_ERROR_INVALID_ARGS,
                                             "Download operation for product %s has %s as application name, not %s",
                                             product_id, op->app_name, app_name);
      return TRUE;
    }

  if (g_strcmp0 (op->download_url, download_url) != 0)
    {
      g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                             FROME_DOWNLOAD_MANAGER_ERROR_INVALID_ARGS,
                                             "Download operation for product %s has %s as download URL, not %s",
                                             product_id, op->download_url, download_url);
      return TRUE;
    }

  /* This method is meant to only cancel the downloading if possible and
   * silently return if it's too late. */
  if (op->state == DOWNLOAD_OP_STATE_DOWNLOADING)
    {
      op->state = DOWNLOAD_OP_STATE_CANCELLING;

      if (op->download_proxy != NULL)
        {
          download_operation_cancel (op);
        }
      /* else: operation will be cancelled in newport_start_download_cb */
    }

  frome_dnld_mgr_complete_cancel_appstore_download (service, invocation);
  return TRUE;
}

static FromeAppDBEntry *
check_app_entry (FromeDownloadManager *self,
                 GDBusMethodInvocation *invocation,
                 const gchar *product_id)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;
  g_autoptr (GError) error = NULL;

  entry = frome_app_db_find_entry (self->app_db, product_id, &error);
  if (entry == NULL)
    {
      if (error == NULL)
        g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                               FROME_DOWNLOAD_MANAGER_ERROR_INVALID_ARGS,
                                               "Product %s is not installed", product_id);
      else
        g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                               FROME_DOWNLOAD_MANAGER_ERROR_DB,
                                               "Failed to fetch product %s from DB: %s",
                                               product_id, error->message);
      return NULL;
    }

  return g_steal_pointer (&entry);
}

static gboolean
on_handle_update_application (FromeDnldMgr *service,
                              GDBusMethodInvocation *invocation,
                              const gchar *app_name,
                              const gchar *download_url,
                              const gchar *product_id,
                              const gchar *app_version,
                              gint package_size,
                              FromeDownloadManager *self)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;

  entry = check_app_entry (self, invocation, product_id);
  if (entry == NULL)
    return TRUE;

  /* TODO: check if @app_version is greater than the current installed version?
   * We'd need a more formal definition of the version field to do so. */

  if (!start_download (self, invocation, app_name, download_url,
                       product_id, app_version, package_size, TRUE))
    return TRUE;

  frome_dnld_mgr_complete_update_application (service, invocation);
  return TRUE;
}

typedef struct
{
  FromeDownloadManager *self;
  FromeAppDBEntry *entry;
  FromeAccount *account;
} PendingOperation;

/* @account: (transfer full) */
static PendingOperation *
pending_operation_new (FromeDownloadManager *self,
                       FromeAppDBEntry *entry,
                       FromeAccount *account)
{
  PendingOperation *op = g_new0 (PendingOperation, 1);

  op->self = g_object_ref (self);
  op->entry = g_object_ref (entry);
  op->account = account;
  return op;
}

static void
pending_operation_free (PendingOperation *op)
{
  g_clear_object (&op->self);
  g_clear_object (&op->entry);
  frome_account_unref (op->account);
  g_free (op);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (PendingOperation, pending_operation_free)

static void
uninstall_cb (GObject *source,
              GAsyncResult *result,
              gpointer user_data)
{
  g_autoptr (PendingOperation) op = user_data;
  g_autoptr (GError) error = NULL;

  if (!frome_ribchester_client_uninstall_finish (FROME_RIBCHESTER_CLIENT (source),
                                                 result, &error))
    {
      frome_dnld_mgr_emit_appstore_download_status (op->self->service,
                                                    frome_app_db_entry_get_app_name (op->entry),
                                                    frome_app_db_entry_get_product_id (op->entry),
                                                    FROME_APPSTORE_SIG_STATE_UNINSTALLED,
                                                    FROME_APPSTORE_DOWNLOAD_STATUS_FAILED,
                                                    FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_UNINSTALL_FAILED);
      return;
    }

  update_app_state (op->self, op->entry,
                    frome_app_db_entry_get_product_id (op->entry),
                    op->account->customer_id, APP_STATE_UNINSTALLED);

  frome_dnld_mgr_emit_appstore_download_status (op->self->service,
                                                frome_app_db_entry_get_app_name (op->entry),
                                                frome_app_db_entry_get_product_id (op->entry),
                                                FROME_APPSTORE_SIG_STATE_UNINSTALLED,
                                                FROME_APPSTORE_DOWNLOAD_STATUS_SUCCESS,
                                                FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_NO_ERROR);
}

static gboolean
on_handle_uninstall_application (FromeDnldMgr *service,
                                 GDBusMethodInvocation *invocation,
                                 const gchar *app_name,
                                 const gchar *product_id,
                                 FromeDownloadManager *self)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;
  g_autoptr (FromeAccount) account = NULL;
  g_autoptr (GError) error = NULL;

  entry = check_app_entry (self, invocation, product_id);
  if (entry == NULL)
    return TRUE;

  if (g_strcmp0 (app_name, frome_app_db_entry_get_app_name (entry)) != 0)
    {
      g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                             FROME_DOWNLOAD_MANAGER_ERROR_INVALID_ARGS,
                                             "Product %s has name %s not %s",
                                             product_id, frome_app_db_entry_get_app_name (entry),
                                             app_name);
      return TRUE;
    }

  account = get_active_user (self, invocation);
  if (account == NULL)
    return TRUE;

  frome_app_db_entry_set_process_type (entry, PROCESS_TYPE_APP_UNINSTALL);
  if (!frome_app_db_entry_save (entry, &error))
    {
      g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                             FROME_DOWNLOAD_MANAGER_ERROR_DB,
                                             "Failed to update DB: %s", error->message);
      return TRUE;
    }

  frome_ribchester_client_uninstall_async (self->ribchester, app_name,
                                           NULL, uninstall_cb,
                                           pending_operation_new (self, entry, g_steal_pointer (&account)));

  frome_dnld_mgr_complete_uninstall_application (service, invocation);
  return TRUE;
}

static void
reinstall_cb (GObject *source,
              GAsyncResult *result,
              gpointer user_data)
{
  g_autoptr (PendingOperation) op = user_data;
  g_autoptr (GError) error = NULL;

  if (!frome_ribchester_client_reinstall_finish (FROME_RIBCHESTER_CLIENT (source),
                                                 result, &error))
    {
      frome_dnld_mgr_emit_appstore_download_status (op->self->service,
                                                    frome_app_db_entry_get_app_name (op->entry),
                                                    frome_app_db_entry_get_product_id (op->entry),
                                                    FROME_APPSTORE_SIG_STATE_REINSTALL,
                                                    FROME_APPSTORE_DOWNLOAD_STATUS_FAILED,
                                                    FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_REINSTALL_FAILED);
      return;
    }

  update_app_state (op->self, op->entry,
                    frome_app_db_entry_get_product_id (op->entry),
                    op->account->customer_id, APP_STATE_INSTALLED);

  frome_dnld_mgr_emit_appstore_download_status (op->self->service,
                                                frome_app_db_entry_get_app_name (op->entry),
                                                frome_app_db_entry_get_product_id (op->entry),
                                                FROME_APPSTORE_SIG_STATE_REINSTALL,
                                                FROME_APPSTORE_DOWNLOAD_STATUS_SUCCESS,
                                                FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_NO_ERROR);
}

static gboolean
on_handle_reinstall_application (FromeDnldMgr *service,
                                 GDBusMethodInvocation *invocation,
                                 const gchar *app_name,
                                 const gchar *product_id,
                                 FromeDownloadManager *self)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;
  g_autoptr (FromeAccount) account = NULL;
  g_autoptr (GError) error = NULL;

  entry = check_app_entry (self, invocation, product_id);
  if (entry == NULL)
    return TRUE;

  if (g_strcmp0 (app_name, frome_app_db_entry_get_app_name (entry)) != 0)
    {
      g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                             FROME_DOWNLOAD_MANAGER_ERROR_INVALID_ARGS,
                                             "Product %s has name %s not %s",
                                             product_id, frome_app_db_entry_get_app_name (entry),
                                             app_name);
      return TRUE;
    }

  account = get_active_user (self, invocation);
  if (account == NULL)
    return TRUE;

  frome_app_db_entry_set_process_type (entry, PROCESS_TYPE_APP_REINSTALL);
  if (!frome_app_db_entry_save (entry, &error))
    {
      g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                             FROME_DOWNLOAD_MANAGER_ERROR_DB,
                                             "Failed to update DB: %s", error->message);
      return TRUE;
    }

  frome_ribchester_client_reinstall_async (self->ribchester, app_name,
                                           NULL, reinstall_cb,
                                           pending_operation_new (self, entry, g_steal_pointer (&account)));

  frome_dnld_mgr_complete_reinstall_application (service, invocation);
  return TRUE;
}

static void
rollback_cb (GObject *source,
             GAsyncResult *result,
             gpointer user_data)
{
  g_autoptr (PendingOperation) op = user_data;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *version = NULL;

  version = frome_ribchester_client_rollback_finish (FROME_RIBCHESTER_CLIENT (source),
                                                     result, &error);
  if (version == NULL)
    {
      g_warning ("Failed to rollback: %s", error->message);
      return;
    }

  frome_app_db_entry_set_version (op->entry, version);
  save_entry (op->entry);

  /* Old Frome didn't seem to fire any signal when rollbacking */
}

static gboolean
on_handle_rollback_application (FromeDnldMgr *service,
                                GDBusMethodInvocation *invocation,
                                const gchar *app_name,
                                const gchar *product_id,
                                FromeDownloadManager *self)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;
  g_autoptr (FromeAccount) account = NULL;
  g_autoptr (GError) error = NULL;

  entry = check_app_entry (self, invocation, product_id);
  if (entry == NULL)
    return TRUE;

  account = get_active_user (self, invocation);
  if (account == NULL)
    return TRUE;

  frome_app_db_entry_set_process_type (entry, PROCESS_TYPE_APP_ROLLBACK);
  if (!frome_app_db_entry_save (entry, &error))
    {
      g_dbus_method_invocation_return_error (invocation, FROME_DOWNLOAD_MANAGER_ERROR,
                                             FROME_DOWNLOAD_MANAGER_ERROR_DB,
                                             "Failed to update DB: %s", error->message);
      return TRUE;
    }

  frome_ribchester_client_rollback_async (self->ribchester, app_name,
                                          NULL, rollback_cb,
                                          pending_operation_new (self, entry, g_steal_pointer (&account)));

  frome_dnld_mgr_complete_rollback_application (service, invocation);
  return TRUE;
}

static void
frome_download_manager_init (FromeDownloadManager *self)
{
  self->service = frome_dnld_mgr_skeleton_new ();
  self->cancellable = g_cancellable_new ();
  self->downloads = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
                                           (GDestroyNotify) download_operation_unref);

  g_signal_connect (self->service, "handle-start-appstore-download",
                    G_CALLBACK (on_handle_start_appstore_download), self);
  g_signal_connect (self->service, "handle-cancel-appstore-download",
                    G_CALLBACK (on_handle_cancel_appstore_download), self);
  g_signal_connect (self->service, "handle-update-application",
                    G_CALLBACK (on_handle_update_application), self);
  g_signal_connect (self->service, "handle-uninstall-application",
                    G_CALLBACK (on_handle_uninstall_application), self);
  g_signal_connect (self->service, "handle-reinstall-application",
                    G_CALLBACK (on_handle_reinstall_application), self);
  g_signal_connect (self->service, "handle-rollback-application",
                    G_CALLBACK (on_handle_rollback_application), self);
}

FromeDownloadManager *
frome_download_manager_new (GDBusConnection *conn,
                            GDBusConnection *sys_conn,
                            FromeWebStoreClient *web_store_client,
                            FromeAccountDB *account_db,
                            const gchar *device_id,
                            FromeAppDB *app_db)
{
  return g_object_new (FROME_TYPE_DOWNLOAD_MANAGER,
                       "connection", conn,
                       "system-connection", sys_conn,
                       "web-store-client", web_store_client,
                       "account-db", account_db,
                       "device-id", device_id,
                       "application-db", app_db,
                       NULL);
}

static void
fail_init_tasks (FromeDownloadManager *self,
                 GError *error)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_error (task, g_error_copy (error));
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
complete_init_tasks (FromeDownloadManager *self)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_boolean (task, TRUE);
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
ribchester_init_cb (GObject *source,
                    GAsyncResult *result,
                    gpointer user_data)
{
  FromeDownloadManager *self = user_data;

  if (!g_async_initable_init_finish (G_ASYNC_INITABLE (source), result,
                                     &self->invalidated_error))
    {
      fail_init_tasks (self, self->invalidated_error);
      g_signal_emit (self, signals[SIG_INVALIDATED], 0, self->invalidated_error);
      return;
    }

  self->registered = TRUE;
  complete_init_tasks (self);
}

static void
newport_init_cb (GObject *source,
                 GAsyncResult *result,
                 gpointer user_data)
{
  FromeDownloadManager *self = user_data;
  GCancellable *cancellable;

  if (!g_async_initable_init_finish (G_ASYNC_INITABLE (source), result,
                                     &self->invalidated_error))
    {
      fail_init_tasks (self, self->invalidated_error);
      g_signal_emit (self, signals[SIG_INVALIDATED], 0, self->invalidated_error);
      return;
    }

  cancellable = g_task_get_cancellable (self->init_tasks->data);

  self->ribchester = frome_ribchester_client_new (self->sys_conn);

  g_async_initable_init_async (G_ASYNC_INITABLE (self->ribchester),
                               G_PRIORITY_DEFAULT, cancellable,
                               ribchester_init_cb, self);
}

static void
bus_name_acquired_cb (GDBusConnection *conn,
                      const gchar *name,
                      gpointer user_data)
{
  FromeDownloadManager *self = user_data;
  GCancellable *cancellable;

  if (self->invalidated_error != NULL)
    return;

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self->service),
                                         self->conn, FROME_DOWNLOAD_MANAGER_PATH,
                                         &self->invalidated_error))
    {
      fail_init_tasks (self, self->invalidated_error);
      g_signal_emit (self, signals[SIG_INVALIDATED], 0, self->invalidated_error);
      return;
    }

  cancellable = g_task_get_cancellable (self->init_tasks->data);

  self->newport = frome_newport_client_new (self->conn);

  g_async_initable_init_async (G_ASYNC_INITABLE (self->newport),
                               G_PRIORITY_DEFAULT, cancellable,
                               newport_init_cb, self);
}

static void
bus_name_lost_cb (GDBusConnection *conn,
                  const gchar *name,
                  gpointer user_data)
{
  FromeDownloadManager *self = user_data;

  if (self->invalidated_error != NULL)
    return;

  self->invalidated_error = g_error_new (G_DBUS_ERROR, G_DBUS_ERROR_NAME_HAS_NO_OWNER, "Lost bus name '%s'", name);

  fail_init_tasks (self, self->invalidated_error);
  g_signal_emit (self, signals[SIG_INVALIDATED], 0, self->invalidated_error);
}

static void
frome_download_manager_init_async (GAsyncInitable *initable,
                                   int io_priority,
                                   GCancellable *cancellable,
                                   GAsyncReadyCallback callback,
                                   gpointer user_data)
{
  FromeDownloadManager *self = FROME_DOWNLOAD_MANAGER (initable);
  g_autoptr (GTask) task = NULL;
  gboolean start_init;

  g_return_if_fail (self->conn != NULL);

  task = g_task_new (initable, cancellable, callback, user_data);

  if (self->registered)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  start_init = (self->init_tasks == NULL);
  self->init_tasks = g_list_append (self->init_tasks, g_object_ref (task));

  if (start_init)
    self->own_name_id = g_bus_own_name_on_connection (self->conn,
                                                      FROME_DOWNLOAD_MANAGER_BUS_NAME,
                                                      G_BUS_NAME_OWNER_FLAGS_NONE,
                                                      bus_name_acquired_cb,
                                                      bus_name_lost_cb, self,
                                                      NULL);
}

static gboolean
frome_download_manager_init_finish (GAsyncInitable *initable,
                                    GAsyncResult *result,
                                    GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, initable), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data)
{
  GAsyncInitableIface *iface = g_iface;

  iface->init_async = frome_download_manager_init_async;
  iface->init_finish = frome_download_manager_init_finish;
}
