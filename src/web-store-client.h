/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FROME_WEB_STORE_CLIENT_H__
#define __FROME_WEB_STORE_CLIENT_H__

#include <gio/gio.h>

#include "src/constants.h"

G_BEGIN_DECLS

#define FROME_TYPE_WEB_STORE_CLIENT (frome_web_store_client_get_type ())
G_DECLARE_FINAL_TYPE (FromeWebStoreClient, frome_web_store_client, FROME, WEB_STORE_CLIENT, GObject)

FromeWebStoreClient *frome_web_store_client_new (void);

void frome_web_store_client_create_account_async (FromeWebStoreClient *self,
                                                  const gchar *device,
                                                  const gchar *first_name,
                                                  const gchar *last_name,
                                                  const gchar *email,
                                                  const gchar *password,
                                                  gboolean order_newsletter,
                                                  const gchar *phone,
                                                  const gchar *street,
                                                  const gchar *city,
                                                  const gchar *postal_code,
                                                  const gchar *state,
                                                  const gchar *country,
                                                  GAsyncReadyCallback callback,
                                                  gpointer user_data);

gchar *frome_web_store_client_create_account_finish (FromeWebStoreClient *self,
                                                     GAsyncResult *result,
                                                     GError **error);

void frome_web_store_client_set_target_id_async (FromeWebStoreClient *self,
                                                 const gchar *device,
                                                 const gchar *customer_id,
                                                 const gchar *target_id,
                                                 AccountType account_type,
                                                 GAsyncReadyCallback callback,
                                                 gpointer user_data);

gboolean frome_web_store_client_set_target_id_finish (FromeWebStoreClient *self,
                                                      GAsyncResult *result,
                                                      GError **error);

void frome_web_store_client_set_target_app_state_async (FromeWebStoreClient *self,
                                                        const gchar *device,
                                                        const gchar *customer_id,
                                                        AccountType account_type,
                                                        GAsyncReadyCallback callback,
                                                        gpointer user_data);

gboolean frome_web_store_client_set_target_app_state_finish (FromeWebStoreClient *self,
                                                             GAsyncResult *result,
                                                             GError **error);

void frome_web_store_client_set_status_async (FromeWebStoreClient *self,
                                              const gchar *device,
                                              const gchar *customer_id,
                                              const gchar *product_id,
                                              AppState status,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data);

AppState frome_web_store_client_set_status_finish (FromeWebStoreClient *self,
                                                   GAsyncResult *result,
                                                   GError **error);

G_END_DECLS

#endif /* __FROME_WEB_STORE_CLIENT_H__ */
