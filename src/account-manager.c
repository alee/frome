/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "account-manager.h"

#include "constants.h"
#include "errors.h"

#include "dbus/Frome.AccountMgr.h"

/* TODO: generate this target ID? */
#define TARGET_ID "apertis-armhf"

#define DEFAULT_ACCOUNT_TYPE ACCOUNT_TYPE_BOTH

/* CreateAccount() params */
#define ACCOUNT_MANAGER_KEY_FIRST_NAME "firstname"
#define ACCOUNT_MANAGER_KEY_LAST_NAME "lastname"
#define ACCOUNT_MANAGER_KEY_EMAIL "e_mail"
#define ACCOUNT_MANAGER_KEY_PASSWORD "password"
#define ACCOUNT_MANAGER_KEY_ORDER_NEWSLETTER "order_newsletter"
#define ACCOUNT_MANAGER_KEY_PHONE "telephone"
#define ACCOUNT_MANAGER_KEY_STREET "streetname_housenumber"
#define ACCOUNT_MANAGER_KEY_CITY "city"
#define ACCOUNT_MANAGER_KEY_POSTAL_CODE "postalcode"
#define ACCOUNT_MANAGER_KEY_STATE "state"
#define ACCOUNT_MANAGER_KEY_COUNTRY "country"

/* Ideally those should be the same as the ACCOUNT_MANAGER_KEY_* but for some
 * reason they are not and we can't change it without breaking the API. */
#define ACCOUNT_STATUS_KEY_FIRST_NAME "Firstname"
#define ACCOUNT_STATUS_KEY_LAST_NAME "Lastname"
#define ACCOUNT_STATUS_KEY_EMAIL_ID "Emailid"
#define ACCOUNT_STATUS_KEY_ACTIVE_USER "ActiveUser"
#define ACCOUNT_STATUS_KEY_DISTRIBUTION "Distribution"
#define ACCOUNT_STATUS_KEY_CUSTOMER_ID "CustomerID"

struct _FromeAccountManager
{
  GObject parent;

  GDBusConnection *conn; /* owned */

  gboolean registered;
  guint own_name_id;                     /* 0 if the server is not registered yet */
  GList /* <owned GTask> */ *init_tasks; /* owned */
  GError *invalidated_error;             /* owned, NULL until the service is invalidated */

  FromeAccountMgr *service;
  FromeWebStoreClient *web;
  FromeAccountDB *db;

  gchar *device;
};

typedef enum {
  PROP_CONNECTION = 1,
  PROP_WEB_STORE_CLIENT,
  PROP_ACCOUNT_DB,
  PROP_DEVICE_ID,
  /*< private >*/
  PROP_LAST = PROP_DEVICE_ID
} FromeAccountManagerProperty;

static GParamSpec *properties[PROP_LAST + 1];

enum
{
  SIG_INVALIDATED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data);

G_DEFINE_TYPE_WITH_CODE (FromeAccountManager, frome_account_manager, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init));

static void
frome_account_manager_get_property (GObject *object,
                                    guint prop_id,
                                    GValue *value,
                                    GParamSpec *pspec)
{
  FromeAccountManager *self = FROME_ACCOUNT_MANAGER (object);

  switch ((FromeAccountManagerProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->conn);
      break;
    case PROP_WEB_STORE_CLIENT:
      g_value_set_object (value, self->web);
      break;
    case PROP_ACCOUNT_DB:
      g_value_set_object (value, self->db);
      break;
    case PROP_DEVICE_ID:
      g_value_set_string (value, self->device);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_account_manager_set_property (GObject *object,
                                    guint prop_id,
                                    const GValue *value,
                                    GParamSpec *pspec)
{
  FromeAccountManager *self = FROME_ACCOUNT_MANAGER (object);

  switch ((FromeAccountManagerProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_assert (self->conn == NULL); /* construct only */
      self->conn = g_value_dup_object (value);
      break;
    case PROP_WEB_STORE_CLIENT:
      g_assert (self->web == NULL); /* construct only */
      self->web = g_value_dup_object (value);
      break;
    case PROP_ACCOUNT_DB:
      g_assert (self->db == NULL); /* construct only */
      self->db = g_value_dup_object (value);
      break;
    case PROP_DEVICE_ID:
      g_assert (self->device == NULL); /* construct only */
      self->device = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_account_manager_dispose (GObject *object)
{
  FromeAccountManager *self = (FromeAccountManager *) object;

  g_assert (self->init_tasks == NULL); /* Init tasks keep a ref on self */

  if (self->own_name_id != 0)
    {
      g_bus_unown_name (self->own_name_id);
      self->own_name_id = 0;
    }

  g_clear_object (&self->service);
  g_clear_object (&self->web);
  g_clear_object (&self->conn);
  g_clear_object (&self->db);

  g_clear_pointer (&self->device, g_free);

  g_clear_error (&self->invalidated_error);

  G_OBJECT_CLASS (frome_account_manager_parent_class)
      ->dispose (object);
}

static void
frome_account_manager_class_init (FromeAccountManagerClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = frome_account_manager_get_property;
  object_class->set_property = frome_account_manager_set_property;
  object_class->dispose = frome_account_manager_dispose;

  /**
   * FromeAccountManager:connection:
   *
   * The #GDBusConnection used by the manager to communicate on the bus.
   */
  properties[PROP_CONNECTION] = g_param_spec_object (
      "connection", "Connection", "GDBusConnection", G_TYPE_DBUS_CONNECTION,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * FromeAccountManager:web-store-client:
   *
   * The #FromeWebStoreClient used by the manager to communicate with the
   * web store.
   */
  properties[PROP_WEB_STORE_CLIENT] = g_param_spec_object (
      "web-store-client", "Web Store Client", "FromeWebStoreClient", FROME_TYPE_WEB_STORE_CLIENT,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * FromeAccountManager:account-db:
   *
   * The #FromeAccountDB used by the manager to store and retrieve accounts information.
   */
  properties[PROP_ACCOUNT_DB] = g_param_spec_object (
      "account-db", "Account DB", "FromeAccountDB", FROME_TYPE_ACCOUNT_DB,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * FromeAccountManager:device-id:
   *
   * The hash of the device ID on which the manager is running
   */
  properties[PROP_DEVICE_ID] = g_param_spec_string (
      "device-id", "Device ID", "Device ID", NULL,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * FromeAccountManager::invalidated:
   * @self: a #FromeAccountManager
   * @error: error which caused @self to be invalidated
   *
   * Emitted when @self is no longer usable.
   * The most common reason for this signal to be emitted is if the
   * underlying D-Bus service name is lost.
   */
  signals[SIG_INVALIDATED] = g_signal_new ("invalidated", FROME_TYPE_ACCOUNT_MANAGER,
                                           G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                           G_TYPE_NONE, 1, G_TYPE_ERROR);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

typedef struct
{
  FromeAccountManager *self;
  gchar *first_name;
  gchar *last_name;
  gchar *email;
} CreateAccountOperation;

static CreateAccountOperation *
create_account_operation_new (FromeAccountManager *self,
                              const gchar *first_name,
                              const gchar *last_name,
                              const gchar *email)
{
  CreateAccountOperation *op = g_new (CreateAccountOperation, 1);

  op->self = g_object_ref (self);
  op->first_name = g_strdup (first_name);
  op->last_name = g_strdup (last_name);
  op->email = g_strdup (email);
  return op;
}

static void
create_account_operation_free (CreateAccountOperation *op)
{
  g_object_unref (op->self);
  g_free (op->first_name);
  g_free (op->last_name);
  g_free (op->email);

  g_free (op);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (CreateAccountOperation, create_account_operation_free)

static void
emit_account_status_fail (FromeAccountManager *self,
                          const gchar *email,
                          const gchar *message)
{
  GVariantBuilder builder;

  /* The old implementation is only including the email in case of failure. */
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{ss}"));
  g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_EMAIL_ID, email);

  frome_account_mgr_emit_account_status (self->service,
                                         g_variant_builder_end (&builder),
                                         FALSE, message);
}

static const gchar *
account_type_to_str (AccountType type)
{
  switch (type)
    {
    case ACCOUNT_TYPE_DEVEL:
      return "devel";
    case ACCOUNT_TYPE_RELEASE:
      return "release";
    case ACCOUNT_TYPE_BOTH:
      return "both";
    default:
      break;
    }

  g_return_val_if_reached (NULL);
}

static gboolean
new_account_should_be_active (FromeAccountManager *self)
{
  g_autoptr (GPtrArray) accounts = NULL;
  guint i;

  accounts = frome_account_db_get_accounts (self->db);

  if (accounts->len == 0)
    /* First account, make it the active one */
    return TRUE;

  /* Make it active it if there is currently no active one */
  for (i = 0; i < accounts->len; i++)
    {
      FromeAccount *account = g_ptr_array_index (accounts, i);

      if (account->active_user)
        return FALSE;
    }

  return TRUE;
}

static void
create_account_cb (GObject *source,
                   GAsyncResult *result,
                   gpointer user_data)
{
  g_autoptr (CreateAccountOperation) op = user_data;
  g_autoptr (GError) error = NULL;
  GVariantBuilder builder;
  g_autofree gchar *customer_id = NULL;
  gboolean active_user = FALSE;
  AccountType account_type = DEFAULT_ACCOUNT_TYPE;

  /* Server replied with the customer_id of the new account */
  customer_id = frome_web_store_client_create_account_finish ((FromeWebStoreClient *) source,
                                                              result, &error);
  if (customer_id == NULL)
    {
      g_assert (error != NULL);
      emit_account_status_fail (op->self, op->email, error->message);
      return;
    }

  active_user = new_account_should_be_active (op->self);

  if (!frome_account_db_add_account (op->self->db, op->email, op->first_name,
                                     op->last_name, customer_id, account_type,
                                     active_user, &error))
    {
      g_assert (error != NULL);
      emit_account_status_fail (op->self, op->email, error->message);
      return;
    }

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{ss}"));
  g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_FIRST_NAME, op->first_name);
  g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_LAST_NAME, op->last_name);
  g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_EMAIL_ID, op->email);
  g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_ACTIVE_USER, active_user ? "true" : "false");
  g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_DISTRIBUTION, account_type_to_str (account_type));
  g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_CUSTOMER_ID, customer_id);

  /* Notify about the newly created account. The error message should be the
   * empty string but that would break the API. */
  frome_account_mgr_emit_account_status (op->self->service,
                                         g_variant_builder_end (&builder), TRUE, "NULL");

  /* Send target-id to the server */
  frome_web_store_client_set_target_id_async (op->self->web, op->self->device,
                                              customer_id, TARGET_ID,
                                              account_type, NULL, NULL);
}

typedef struct
{
  gpointer value;
  const gchar *key_id;
  GType type;
  gboolean found;
} KeyMapping;

static gboolean
on_handle_create_account (FromeAccountMgr *service,
                          GDBusMethodInvocation *invocation,
                          GVariant *account_info,
                          FromeAccountManager *self)
{
  g_autoptr (GVariantIter) iter = NULL;
  g_autoptr (GError) error = NULL;
  const gchar *key, *value;
  const gchar *first_name = NULL, *last_name = NULL, *email = NULL,
              *password = NULL, *phone = NULL, *street = NULL, *city = NULL,
              *postal_code = NULL, *state = NULL, *country = NULL;
  gboolean order_newsletter;
  KeyMapping mapping[] = {
    { &first_name, ACCOUNT_MANAGER_KEY_FIRST_NAME, G_TYPE_STRING, FALSE },
    { &last_name, ACCOUNT_MANAGER_KEY_LAST_NAME, G_TYPE_STRING, FALSE },
    { &email, ACCOUNT_MANAGER_KEY_EMAIL, G_TYPE_STRING, FALSE },
    { &order_newsletter, ACCOUNT_MANAGER_KEY_ORDER_NEWSLETTER, G_TYPE_BOOLEAN, FALSE },
    { &password, ACCOUNT_MANAGER_KEY_PASSWORD, G_TYPE_STRING, FALSE },
    { &phone, ACCOUNT_MANAGER_KEY_PHONE, G_TYPE_STRING, FALSE },
    { &street, ACCOUNT_MANAGER_KEY_STREET, G_TYPE_STRING, FALSE },
    { &city, ACCOUNT_MANAGER_KEY_CITY, G_TYPE_STRING, FALSE },
    { &postal_code, ACCOUNT_MANAGER_KEY_POSTAL_CODE, G_TYPE_STRING, FALSE },
    { &state, ACCOUNT_MANAGER_KEY_STATE, G_TYPE_STRING, FALSE },
    { &country, ACCOUNT_MANAGER_KEY_COUNTRY, G_TYPE_STRING, FALSE },
  };
  guint i;
  CreateAccountOperation *op;

  g_variant_get (account_info, "a{ss}", &iter);
  while (g_variant_iter_next (iter, "{&s&s}", &key, &value))
    {
      for (i = 0; i < G_N_ELEMENTS (mapping); i++)
        {
          if (g_strcmp0 (key, mapping[i].key_id) != 0)
            continue;

          switch (mapping[i].type)
            {
            case G_TYPE_STRING:
              *(const gchar **) mapping[i].value = value;
              break;
            case G_TYPE_BOOLEAN:
              /* This should really be a boolean variant... */
              if (g_strcmp0 (value, "YES") == 0)
                *(gboolean *) mapping[i].value = TRUE;
              else if (g_strcmp0 (value, "NO") == 0)
                *(gboolean *) mapping[i].value = FALSE;
              else
                {
                  g_set_error (&error, FROME_ACCOUNT_MANAGER_ERROR,
                               FROME_ACCOUNT_MANAGER_ERROR_INVALID_ARGS,
                               "Invalid value for %s: %s", key, value);
                  goto error;
                }
              break;
            default:
              g_assert_not_reached ();
            }

          mapping[i].found = TRUE;
        }
    }

  for (i = 0; i < G_N_ELEMENTS (mapping); i++)
    {
      if (!mapping[i].found)
        {
          g_set_error (&error, FROME_ACCOUNT_MANAGER_ERROR,
                       FROME_ACCOUNT_MANAGER_ERROR_MISSING_DETAILS,
                       "%s is missing", mapping[i].key_id);
          goto error;
        }
    }

  /* Need to keep some fields around to update the DB in create_account_cb */
  op = create_account_operation_new (self, first_name, last_name, email);

  frome_web_store_client_create_account_async (self->web, self->device, first_name, last_name,
                                               email, password, order_newsletter,
                                               phone, street, city, postal_code,
                                               state, country, create_account_cb,
                                               op);

  /* D-Bus method returns right away. Sucess/failure is notified using
   * the AccountStatus signal */
  frome_account_mgr_complete_create_account (service, invocation);
  return TRUE;

error:
  g_dbus_method_invocation_take_error (invocation, g_steal_pointer (&error));
  return TRUE;
}

static gboolean
on_handle_delete_account (FromeAccountMgr *service,
                          GDBusMethodInvocation *invocation,
                          const gchar *email,
                          FromeAccountManager *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GPtrArray) accounts = NULL;
  gint error_code = FROME_ACCOUNT_MANAGER_ERROR_DB_ERROR;

  if (!frome_account_db_remove_account (self->db, email, &error))
    {
      if (g_error_matches (error, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_GROUP_NOT_FOUND))
        error_code = FROME_ACCOUNT_MANAGER_ERROR_ACCOUNT_NOT_FOUND;

      goto error;
    }

  accounts = frome_account_db_get_accounts (self->db);
  if (accounts->len == 1)
    {
      /* If there is only one account left in the DB, ensure it's active */
      FromeAccount *account = g_ptr_array_index (accounts, 0);

      if (!frome_account_db_update_active_user (self->db, account->email, TRUE, &error))
        goto error;
    }

  frome_account_mgr_complete_delete_account (service, invocation);
  return TRUE;

error:
  g_assert (error != NULL);
  g_dbus_method_invocation_return_error_literal (invocation, FROME_ACCOUNT_MANAGER_ERROR,
                                                 error_code, error->message);
  return TRUE;
}

static gboolean
on_handle_get_accounts (FromeAccountMgr *service,
                        GDBusMethodInvocation *invocation,
                        FromeAccountManager *self)
{
  GVariantBuilder result;
  g_autoptr (GPtrArray) accounts = NULL;
  guint i;

  g_variant_builder_init (&result, G_VARIANT_TYPE ("aa{ss}"));

  accounts = frome_account_db_get_accounts (self->db);
  for (i = 0; i < accounts->len; i++)
    {
      FromeAccount *account = g_ptr_array_index (accounts, i);
      GVariantBuilder builder;

      g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{ss}"));
      g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_FIRST_NAME, account->first_name);
      g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_LAST_NAME, account->last_name);
      g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_EMAIL_ID, account->email);
      g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_ACTIVE_USER, account->active_user ? "true" : "false");
      g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_DISTRIBUTION, account_type_to_str (account->type));
      g_variant_builder_add (&builder, "{ss}", ACCOUNT_STATUS_KEY_CUSTOMER_ID, account->customer_id);

      g_variant_builder_add_value (&result, g_variant_builder_end (&builder));
    }

  frome_account_mgr_complete_get_accounts (service, invocation,
                                           g_variant_builder_end (&result));
  return TRUE;
}

static gboolean
on_handle_set_default_accounts (FromeAccountMgr *service,
                                GDBusMethodInvocation *invocation,
                                const gchar *email,
                                FromeAccountManager *self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GPtrArray) accounts = NULL;
  gint error_code = FROME_ACCOUNT_MANAGER_ERROR_DB_ERROR;
  guint i;

  if (!frome_account_db_update_active_user (self->db, email, TRUE, &error))
    {
      if (g_error_matches (error, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_GROUP_NOT_FOUND))
        error_code = FROME_ACCOUNT_MANAGER_ERROR_ACCOUNT_NOT_FOUND;

      goto error;
    }

  /* Unset the old active account */
  accounts = frome_account_db_get_accounts (self->db);
  for (i = 0; i < accounts->len; i++)
    {
      FromeAccount *account = g_ptr_array_index (accounts, i);

      if (!account->active_user)
        continue;

      if (g_strcmp0 (account->email, email) == 0)
        continue;

      if (!frome_account_db_update_active_user (self->db, account->email, FALSE, &error))
        goto error;
    }

  frome_account_mgr_complete_set_default_account (service, invocation);
  return TRUE;

error:
  g_assert (error != NULL);
  g_dbus_method_invocation_return_error_literal (invocation, FROME_ACCOUNT_MANAGER_ERROR,
                                                 error_code, error->message);
  return TRUE;
}

static gboolean
account_type_from_str (const gchar *account_type,
                       AccountType *type)
{
  gboolean found = TRUE;

  if (g_strcmp0 (account_type, "devel") == 0)
    *type = ACCOUNT_TYPE_DEVEL;
  else if (g_strcmp0 (account_type, "release") == 0)
    *type = ACCOUNT_TYPE_RELEASE;
  else if (g_strcmp0 (account_type, "both") == 0)
    *type = ACCOUNT_TYPE_BOTH;
  else
    found = FALSE;

  return found;
}

typedef struct
{
  FromeAccountManager *self;
  gchar *email;
  AccountType type;
} SetAccountTypeOperation;

static SetAccountTypeOperation *
set_account_type_operation_new (FromeAccountManager *self,
                                const gchar *email,
                                AccountType type)
{
  SetAccountTypeOperation *op = g_new (SetAccountTypeOperation, 1);

  op->self = g_object_ref (self);
  op->email = g_strdup (email);
  op->type = type;
  return op;
}

static void
set_account_type_operation_free (SetAccountTypeOperation *op)
{
  g_object_unref (op->self);
  g_free (op->email);

  g_free (op);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (SetAccountTypeOperation, set_account_type_operation_free)

static void
set_target_app_state_cb (GObject *source,
                         GAsyncResult *result,
                         gpointer user_data)
{
  g_autoptr (SetAccountTypeOperation) op = user_data;
  g_autoptr (GError) error = NULL;

  if (!frome_web_store_client_set_target_app_state_finish (FROME_WEB_STORE_CLIENT (source),
                                                           result, &error))
    {
      g_warning ("web request failed: %s", error->message);
      return;
    }

  if (!frome_account_db_update_account_type (op->self->db, op->email,
                                             op->type, &error))
    {
      g_warning ("failed to update DB: %s", error->message);
      return;
    }

  /* Success or failure isn't reported by the D-Bus API... */
}

static gboolean
on_handle_set_account_type (FromeAccountMgr *service,
                            GDBusMethodInvocation *invocation,
                            const gchar *email,
                            const gchar *type,
                            FromeAccountManager *self)
{
  AccountType account_type;
  g_autoptr (FromeAccount) account = NULL;

  if (!account_type_from_str (type, &account_type))
    {
      g_dbus_method_invocation_return_error (invocation, FROME_ACCOUNT_MANAGER_ERROR,
                                             FROME_ACCOUNT_MANAGER_ERROR_INVALID_ACCOUNT_TYPE,
                                             "Invalid account type '%s'", type);
      return TRUE;
    }

  account = frome_account_db_get_account (self->db, email);
  if (account == NULL)
    {
      g_dbus_method_invocation_return_error (invocation, FROME_ACCOUNT_MANAGER_ERROR,
                                             FROME_ACCOUNT_MANAGER_ERROR_ACCOUNT_NOT_FOUND,
                                             "Account not found: '%s'", email);
      return TRUE;
    }

  /* The DB will be updated in set_target_app_state_cb */
  frome_web_store_client_set_target_app_state_async (self->web, self->device,
                                                     account->customer_id, account_type,
                                                     set_target_app_state_cb,
                                                     set_account_type_operation_new (self, email, account_type));

  frome_account_mgr_complete_set_account_type (service, invocation);
  return TRUE;
}

static void
frome_account_manager_init (FromeAccountManager *self)
{
  self->service = frome_account_mgr_skeleton_new ();

  g_signal_connect (self->service, "handle-create-account",
                    G_CALLBACK (on_handle_create_account), self);
  g_signal_connect (self->service, "handle-delete-account",
                    G_CALLBACK (on_handle_delete_account), self);
  g_signal_connect (self->service, "handle-get-accounts",
                    G_CALLBACK (on_handle_get_accounts), self);
  g_signal_connect (self->service, "handle-set-default-account",
                    G_CALLBACK (on_handle_set_default_accounts), self);
  g_signal_connect (self->service, "handle-set-account-type",
                    G_CALLBACK (on_handle_set_account_type), self);
}

FromeAccountManager *
frome_account_manager_new (GDBusConnection *conn,
                           FromeWebStoreClient *web_store_client,
                           FromeAccountDB *db,
                           const gchar *device_id)
{
  return g_object_new (FROME_TYPE_ACCOUNT_MANAGER,
                       "connection", conn,
                       "web-store-client", web_store_client,
                       "account-db", db,
                       "device-id", device_id,
                       NULL);
}

static void
fail_init_tasks (FromeAccountManager *self,
                 GError *error)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_error (task, g_error_copy (error));
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
complete_init_tasks (FromeAccountManager *self)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_boolean (task, TRUE);
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
bus_name_acquired_cb (GDBusConnection *conn,
                      const gchar *name,
                      gpointer user_data)
{
  FromeAccountManager *self = user_data;

  g_clear_error (&self->invalidated_error);

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self->service),
                                         self->conn, FROME_ACCOUNT_MANAGER_PATH,
                                         &self->invalidated_error))
    {
      fail_init_tasks (self, self->invalidated_error);
      g_signal_emit (self, signals[SIG_INVALIDATED], 0, self->invalidated_error);
      return;
    }

  self->registered = TRUE;
  complete_init_tasks (self);
}

static void
bus_name_lost_cb (GDBusConnection *conn,
                  const gchar *name,
                  gpointer user_data)
{
  FromeAccountManager *self = user_data;

  g_clear_error (&self->invalidated_error);
  self->invalidated_error = g_error_new (G_DBUS_ERROR, G_DBUS_ERROR_NAME_HAS_NO_OWNER, "Lost bus name '%s'", name);

  fail_init_tasks (self, self->invalidated_error);
  g_signal_emit (self, signals[SIG_INVALIDATED], 0, self->invalidated_error);
}

static void
frome_account_manager_init_async (GAsyncInitable *initable,
                                  int io_priority,
                                  GCancellable *cancellable,
                                  GAsyncReadyCallback callback,
                                  gpointer user_data)
{
  FromeAccountManager *self = FROME_ACCOUNT_MANAGER (initable);
  g_autoptr (GTask) task = NULL;
  gboolean start_init;

  g_return_if_fail (self->conn != NULL);

  task = g_task_new (initable, cancellable, callback, user_data);

  if (self->registered)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  start_init = (self->init_tasks == NULL);
  self->init_tasks = g_list_append (self->init_tasks, g_object_ref (task));

  if (start_init)
    self->own_name_id = g_bus_own_name_on_connection (self->conn,
                                                      FROME_ACCOUNT_MANAGER_BUS_NAME,
                                                      G_BUS_NAME_OWNER_FLAGS_NONE,
                                                      bus_name_acquired_cb,
                                                      bus_name_lost_cb, self,
                                                      NULL);
}

static gboolean
frome_account_manager_init_finish (GAsyncInitable *initable,
                                   GAsyncResult *result,
                                   GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, initable), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data)
{
  GAsyncInitableIface *iface = g_iface;

  iface->init_async = frome_account_manager_init_async;
  iface->init_finish = frome_account_manager_init_finish;
}
