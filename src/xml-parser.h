/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FROME_XML_PARSER_H__
#define __FROME_XML_PARSER_H__

#include <gio/gio.h>

#include "constants.h"

G_BEGIN_DECLS

gboolean frome_xml_parser_parse_customer (const gchar *data,
                                          gssize len,
                                          gchar **customer_id,
                                          GError **error);

gboolean frome_xml_parser_parse_status (const gchar *data,
                                        gssize len,
                                        AppState *status,
                                        GError **error);

G_END_DECLS

#endif /* __FROME_XML_PARSER_H__ */
