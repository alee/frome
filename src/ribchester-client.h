/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FROME_RIBCHESTER_CLIENT_H__
#define __FROME_RIBCHESTER_CLIENT_H__

#include <gio/gio.h>

#define FROME_TYPE_RIBCHESTER_CLIENT (frome_ribchester_client_get_type ())
G_DECLARE_FINAL_TYPE (FromeRibchesterClient, frome_ribchester_client, FROME, RIBCHESTER_CLIENT, GObject)

FromeRibchesterClient *frome_ribchester_client_new (GDBusConnection *conn);

void frome_ribchester_client_install_async (FromeRibchesterClient *self,
                                            const gchar *path,
                                            GCancellable *cancellable,
                                            GAsyncReadyCallback callback,
                                            gpointer user_data);

gboolean frome_ribchester_client_install_finish (FromeRibchesterClient *self,
                                                 GAsyncResult *result,
                                                 GError **error);

void frome_ribchester_client_uninstall_async (FromeRibchesterClient *self,
                                              const gchar *bundle_id,
                                              GCancellable *cancellable,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data);

gboolean frome_ribchester_client_uninstall_finish (FromeRibchesterClient *self,
                                                   GAsyncResult *result,
                                                   GError **error);

void frome_ribchester_client_reinstall_async (FromeRibchesterClient *self,
                                              const gchar *bundle_id,
                                              GCancellable *cancellable,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data);

gboolean frome_ribchester_client_reinstall_finish (FromeRibchesterClient *self,
                                                   GAsyncResult *result,
                                                   GError **error);

void frome_ribchester_client_rollback_async (FromeRibchesterClient *self,
                                             const gchar *bundle_id,
                                             GCancellable *cancellable,
                                             GAsyncReadyCallback callback,
                                             gpointer user_data);

gchar *frome_ribchester_client_rollback_finish (FromeRibchesterClient *self,
                                                GAsyncResult *result,
                                                GError **error);

G_END_DECLS

#endif /* __FROME_RIBCHESTER_CLIENT_H__ */
