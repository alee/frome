/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "newport-client.h"

#define NEWPORT_BUS_NAME "org.apertis.Newport"
#define NEWPORT_PATH "/org/apertis/Newport/Service"

struct _FromeNewportClient
{
  GObject parent;

  GDBusConnection *conn; /* owned */

  gboolean initialized;
  GList /* <owned GTask> */ *init_tasks; /* owned */

  NewportService *proxy;
};

typedef enum {
  PROP_CONNECTION = 1,
  /*< private >*/
  PROP_LAST = PROP_CONNECTION
} FromeNewportClientProperty;

static GParamSpec *properties[PROP_LAST + 1];

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data);

G_DEFINE_TYPE_WITH_CODE (FromeNewportClient, frome_newport_client, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init));

static void
frome_newport_client_get_property (GObject *object,
                                   guint prop_id,
                                   GValue *value,
                                   GParamSpec *pspec)
{
  FromeNewportClient *self = FROME_NEWPORT_CLIENT (object);

  switch ((FromeNewportClientProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->conn);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_newport_client_set_property (GObject *object,
                                   guint prop_id,
                                   const GValue *value,
                                   GParamSpec *pspec)
{
  FromeNewportClient *self = FROME_NEWPORT_CLIENT (object);

  switch ((FromeNewportClientProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_assert (self->conn == NULL); /* construct only */
      self->conn = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
frome_newport_client_dispose (GObject *object)
{
  FromeNewportClient *self = (FromeNewportClient *) object;

  g_clear_object (&self->conn);

  g_clear_object (&self->proxy);

  G_OBJECT_CLASS (frome_newport_client_parent_class)
      ->dispose (object);
}

static void
frome_newport_client_class_init (FromeNewportClientClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = frome_newport_client_get_property;
  object_class->set_property = frome_newport_client_set_property;
  object_class->dispose = frome_newport_client_dispose;

  properties[PROP_CONNECTION] = g_param_spec_object (
      "connection", "Connection", "GDBusConnection", G_TYPE_DBUS_CONNECTION,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
frome_newport_client_init (FromeNewportClient *self)
{
}

FromeNewportClient *
frome_newport_client_new (GDBusConnection *conn)
{
  return g_object_new (FROME_TYPE_NEWPORT_CLIENT,
                       "connection", conn,
                       NULL);
}

static void
fail_init_tasks (FromeNewportClient *self,
                 GError *error)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_error (task, g_error_copy (error));
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
complete_init_tasks (FromeNewportClient *self)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_boolean (task, TRUE);
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
newport_proxy_cb (GObject *source,
                  GAsyncResult *result,
                  gpointer user_data)
{
  FromeNewportClient *self = user_data;
  g_autoptr (GError) error = NULL;

  self->proxy = newport_service_proxy_new_finish (result, &error);
  if (self->proxy == NULL)
    {
      fail_init_tasks (self, error);
      return;
    }

  self->initialized = TRUE;
  complete_init_tasks (self);
}

static void
frome_download_manager_init_async (GAsyncInitable *initable,
                                   int io_priority,
                                   GCancellable *cancellable,
                                   GAsyncReadyCallback callback,
                                   gpointer user_data)
{
  FromeNewportClient *self = FROME_NEWPORT_CLIENT (initable);
  g_autoptr (GTask) task = NULL;
  gboolean start_init;

  g_return_if_fail (self->conn != NULL);

  task = g_task_new (initable, cancellable, callback, user_data);

  if (self->initialized)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  start_init = (self->init_tasks == NULL);
  self->init_tasks = g_list_append (self->init_tasks, g_object_ref (task));

  if (start_init)
    newport_service_proxy_new (self->conn, G_DBUS_PROXY_FLAGS_NONE,
                               NEWPORT_BUS_NAME, NEWPORT_PATH, cancellable,
                               newport_proxy_cb, self);
}

static gboolean
frome_download_manager_init_finish (GAsyncInitable *initable,
                                    GAsyncResult *result,
                                    GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, initable), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data)
{
  GAsyncInitableIface *iface = g_iface;

  iface->init_async = frome_download_manager_init_async;
  iface->init_finish = frome_download_manager_init_finish;
}

static void
download_proxy_cb (GObject *source,
                   GAsyncResult *result,
                   gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (NewportDownloadProxy) download = NULL;

  download = (NewportDownloadProxy *) newport_download_proxy_new_finish (result, &error);
  if (download == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_return_pointer (task, g_steal_pointer (&download), g_object_unref);
}

static void
start_download_cb (GObject *source,
                   GAsyncResult *result,
                   gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  FromeNewportClient *self = g_task_get_source_object (task);
  g_autoptr (GError) error = NULL;
  g_autofree gchar *object_path = NULL;
  GCancellable *cancellable = g_task_get_cancellable (task);

  if (!newport_service_call_start_download_finish (NEWPORT_SERVICE (source),
                                                   &object_path, result,
                                                   &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  newport_download_proxy_new (self->conn, G_DBUS_PROXY_FLAGS_NONE, NEWPORT_BUS_NAME,
                              object_path, cancellable, download_proxy_cb,
                              g_steal_pointer (&task));
}

void
frome_newport_client_start_download_async (FromeNewportClient *self,
                                           const gchar *url,
                                           const gchar *path,
                                           GCancellable *cancellable,
                                           GAsyncReadyCallback callback,
                                           gpointer user_data)
{
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);

  newport_service_call_start_download (self->proxy, url, path, cancellable,
                                       start_download_cb, g_steal_pointer (&task));
}

NewportDownload *
frome_newport_client_start_download_finish (FromeNewportClient *self,
                                            GAsyncResult *result,
                                            GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_pointer (G_TASK (result), error);
}
