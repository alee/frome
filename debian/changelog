frome (0.2020.1) apertis; urgency=medium

  * Switch to native format to work with the GitLab-to-OBS pipeline.
  * gitlab-ci: Link to the Apertis GitLab CI pipeline definition.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Mon, 27 Jan 2020 16:49:35 +0800

frome (0.2019.0-0co3) apertis; urgency=medium

  * debian/control: Depend on the hotdoc-0.8 branch

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 08 Jan 2020 16:02:08 +0000

frome (0.2019.0-0co2) apertis; urgency=medium

  * debian/control: add dh-apparmor as build-deps.
  * debian/rules: call dh_apparmor for each profile that we ship in the
    binary packages.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Thu, 31 Oct 2019 14:42:43 +0800

frome (0.2019.0-0co1) apertis; urgency=medium

  * Fix GCC warning causing the build to fail with -Werror
  * debian/gbp.conf: Fix spurious escape character

 -- Emanuele Aina <emanuele.aina@collabora.com>  Thu, 22 Aug 2019 23:36:31 +0000

frome (0.1706.0-0co2) apertis; urgency=medium

  * Make compiler warnings non-fatal.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Tue, 19 Mar 2019 16:07:22 +0100

frome (0.1706.0-0co1) 17.06; urgency=medium

  [ Thushara Malali Somesha ]
  * Add missing compiler and preprocessor flags for code coverage

 -- Simon McVittie <smcv@collabora.com>  Fri, 14 Apr 2017 15:15:36 +0100

frome (0.1703.0-0co1) 17.03; urgency=medium

  [ Luis Araujo ]
  * Add frome debian autopkgtest

  [ Nandini Raju ]
  * Corrected App Download Progress state (Apertis: T3421)

  [ Manoj Prabhakar Nallabothula ]
  * Add apparmor rule for mildenhall-settings to talk with frome

  [ Guillaume Desmottes ]
  * service: expose the device ID in the D-Bus API
  * account-manager: expose the customer-id in the D-Bus API

 -- Justin Kim <justin.kim@collabora.com>  Fri, 03 Mar 2017 14:59:59 +0900

frome (0.1612.3-0co1) 16.12; urgency=medium

  * apparmor: stop using -strict abstractions

 -- Guillaume Desmottes <guillaume.desmottes@collabora.co.uk>  Thu, 15 Dec 2016 12:42:13 +0000

frome (0.1612.2-0co2) 16.12; urgency=medium

  [ Guillaume Desmottes ]
  * debian: update symbols file

 -- Guillaume Desmottes <guillaume.desmottes@collabora.co.uk>  Thu, 08 Dec 2016 10:39:43 +0000

frome (0.1612.2-0co1) 16.12; urgency=medium

  [ Guillaume Desmottes ]
  * debian: add hotdoc 0.8 as build dep
  * apparmor: allow Frome to query the proxy service
  * dbus: add Frome.DnldMgr.xml and generate code
  * add libfrome
  * coding style fix
  * arcling: check coding style in frome/
  * frome-tool: add commands to order apps and get links
  * service: move device id definition to FromeService object
  * download-manager: start implementing service
  * add FromeAppDB
  * service: create an application DB and pass it to the download-manager
  * web-store-client: add API to send application status
  * account-db: add frome_account_db_get_active_user()
  * add newport-client
  * mock-web-store-server: add ability to serve bundle files
  * use an internal static lib
  * debian: rename 'frome-tool' package to 'frome-tools'
  * add frome-dump-app-db
  * add ribchester-client
  * rename and generate a second test bundle
  * download-manager: implement StartAppstoreDownload()
  * download-manager: implement CancelAppstoreDownload() (Apertis: T2937)
  * download-manager: implement Frome.DnldMgr.Update() (Apertis: T2938)
  * download-manager: implement Frome.DnldMgr.Uninstall()
  * download-manager: implement Frome.DnldMgr.Reinstall()
  * download-manager: implement Frome.DnldMgr.RollbackApplication()
    (Apertis: T2939)
  * frome-mock-web-server: add copyright header
  * frome-tool: apparmor: allow to use nameservice
  * org.apertis.Frome.xml: replace UNRELEASED
  * Finish the re-implementation of the legacy Frome D-Bus interfaces (Apertis: T2932)

 -- Guillaume Desmottes <guillaume.desmottes@collabora.co.uk>  Thu, 08 Dec 2016 08:55:11 +0000

frome (0.1612.1-0co2) 16.12; urgency=medium

  [ Guillaume Desmottes ]
  * debian: add dep on libsoup
  * debian: depend on python3-gi

 -- Guillaume Desmottes <guillaume.desmottes@collabora.co.uk>  Fri, 11 Nov 2016 12:45:46 +0000

frome (0.1612.1-0co1) 16.12; urgency=medium

  [ Guillaume Desmottes ]
  * Pre release build. (Apertis: T2933)
  * fix test-service apparmor profile
  * dbus: generate code for the account manager D-Bus API
  * add FromeWebStoreClient
  * account-manager: start implementing account manager API
  * add frome-mock-web-store-server
  * add D-Bus API to configure the web store URL
  * add FromeXmlParser
  * add account manager D-Bus errors
  * add FromeAccountDB
  * add D-Bus API to overwrite the DB account path
  * account-manager: implement Frome.AccountMgr.CreateAccount()
  * add frome-tool
  * account-manager: implement Frome.AccountMgr.DeleteAccount()
  * account-manager: implement Frome.AccountMgr.GetAccounts()
  * account-manager: implement Frome.AccountMgr.SetDefaultAccount()
  * account-manager: implement Frome.AccountMgr.SetAccountType()
    (Apertis: T2936)

 -- Guillaume Desmottes <guillaume.desmottes@collabora.co.uk>  Fri, 11 Nov 2016 10:12:46 +0100
