/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib/gstdio.h>

#include "src/account-db.h"

typedef struct
{
  FromeAccountDB *db;

  gchar *dir;
  gchar *path;
  GError *error;
} Test;

static void
setup (Test *test, gconstpointer unused)
{
  test->dir = g_dir_make_tmp ("frome-test-account-XXXXXX", &test->error);
  g_assert_no_error (test->error);

  test->path = g_build_filename (test->dir, "db.ini", NULL);

  test->db = frome_account_db_new (test->path);
  g_assert (FROME_IS_ACCOUNT_DB (test->db));
}

static void
teardown (Test *test, gconstpointer unused)
{
  g_clear_object (&test->db);

  if (test->path != NULL)
    {
      g_unlink (test->path);
      g_clear_pointer (&test->path, g_free);
    }

  if (test->dir != NULL)
    {
      g_rmdir (test->dir);
      g_clear_pointer (&test->dir, g_free);
    }

  g_clear_error (&test->error);
}

static void
check_account (FromeAccount *account)
{
  g_assert (account != NULL);
  g_assert_cmpstr (account->email, ==, "mail1@test.com");
  g_assert_cmpstr (account->first_name, ==, "first1");
  g_assert_cmpstr (account->last_name, ==, "last1");
  g_assert_cmpstr (account->customer_id, ==, "customer1");
  g_assert_cmpuint (account->type, ==, ACCOUNT_TYPE_DEVEL);
  g_assert_true (account->active_user);
}

static void
test_add_account (Test *test,
                  gconstpointer unused)
{
  gboolean result;
  g_autoptr (FromeAccountDB) db = NULL;
  g_autoptr (GPtrArray) accounts = NULL;
  g_autoptr (FromeAccount) account = NULL;

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->db), ==, 0);

  result = frome_account_db_add_account (test->db, "mail1@test.com", "first1",
                                         "last1", "customer1", ACCOUNT_TYPE_DEVEL,
                                         TRUE, &test->error);
  g_assert_no_error (test->error);
  g_assert_true (result);

  /* Create a new DB with the same path to test loading from file */
  db = frome_account_db_new (test->path);
  accounts = frome_account_db_get_accounts (db);

  g_assert (accounts != NULL);
  g_assert_cmpuint (accounts->len, ==, 1);

  check_account (g_ptr_array_index (accounts, 0));

  account = frome_account_db_get_account (test->db, "mail1@test.com");
  check_account (account);

  g_assert_cmpuint (frome_account_db_get_n_accounts (db), ==, 1);

  /* Trying to re-add the same account fails */
  result = frome_account_db_add_account (test->db, "mail1@test.com", "first1",
                                         "last1", "customer1", ACCOUNT_TYPE_DEVEL,
                                         TRUE, &test->error);
  g_assert_error (test->error, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_INVALID_VALUE);
  g_assert_false (result);
}

static void
test_remove_account (Test *test,
                     gconstpointer unused)
{
  gboolean result;
  g_autoptr (FromeAccount) account1 = NULL, account2 = NULL;
  g_autoptr (FromeAccountDB) db = NULL;

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->db), ==, 0);

  /* Add two accounts */
  result = frome_account_db_add_account (test->db, "mail1@test.com", "first1",
                                         "last1", "customer1", ACCOUNT_TYPE_DEVEL,
                                         TRUE, &test->error);
  g_assert_no_error (test->error);
  g_assert_true (result);

  result = frome_account_db_add_account (test->db, "mail2@test.com", "first2",
                                         "last2", "customer2", ACCOUNT_TYPE_RELEASE,
                                         FALSE, &test->error);
  g_assert_no_error (test->error);
  g_assert_true (result);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->db), ==, 2);

  /* Remove the first one */
  result = frome_account_db_remove_account (test->db, "mail1@test.com", &test->error);
  g_assert_no_error (test->error);
  g_assert_true (result);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->db), ==, 1);

  account1 = frome_account_db_get_account (test->db, "mail1@test.com");
  g_assert (account1 == NULL);

  account1 = frome_account_db_get_account (test->db, "mail2@test.com");
  g_assert (account1 != NULL);

  /* Try removing an unexisting account */
  result = frome_account_db_remove_account (test->db, "badger@test.com", &test->error);
  g_assert_error (test->error, G_KEY_FILE_ERROR, G_KEY_FILE_ERROR_GROUP_NOT_FOUND);
  g_assert_false (result);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->db), ==, 1);

  /* Reload DB */
  db = frome_account_db_new (test->path);

  g_assert_cmpuint (frome_account_db_get_n_accounts (db), ==, 1);

  account2 = frome_account_db_get_account (db, "mail1@test.com");
  g_assert (account2 == NULL);

  account2 = frome_account_db_get_account (db, "mail2@test.com");
  g_assert (account2 != NULL);
}

static void
test_update_active_user (Test *test,
                         gconstpointer unused)
{
  gboolean result;
  g_autoptr (FromeAccountDB) db = NULL;
  g_autoptr (FromeAccount) account = NULL;

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->db), ==, 0);

  result = frome_account_db_add_account (test->db, "mail1@test.com", "first1",
                                         "last1", "customer1", ACCOUNT_TYPE_DEVEL,
                                         FALSE, &test->error);
  g_assert_no_error (test->error);
  g_assert_true (result);

  result = frome_account_db_update_active_user (test->db, "mail1@test.com", TRUE, &test->error);
  g_assert_no_error (test->error);
  g_assert_true (result);

  /* Reload DB */
  db = frome_account_db_new (test->path);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->db), ==, 1);

  account = frome_account_db_get_account (db, "mail1@test.com");
  g_assert (account != NULL);
  g_assert_true (account->active_user);
}

static void
test_update_account_type (Test *test,
                          gconstpointer unused)
{
  gboolean result;
  g_autoptr (FromeAccountDB) db = NULL;
  g_autoptr (FromeAccount) account = NULL;

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->db), ==, 0);

  result = frome_account_db_add_account (test->db, "mail1@test.com", "first1",
                                         "last1", "customer1", ACCOUNT_TYPE_DEVEL,
                                         FALSE, &test->error);
  g_assert_no_error (test->error);
  g_assert_true (result);

  result = frome_account_db_update_account_type (test->db, "mail1@test.com", ACCOUNT_TYPE_RELEASE, &test->error);
  g_assert_no_error (test->error);
  g_assert_true (result);

  /* Reload DB */
  db = frome_account_db_new (test->path);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->db), ==, 1);

  account = frome_account_db_get_account (db, "mail1@test.com");
  g_assert (account != NULL);
  g_assert_cmpuint (account->type, ==, ACCOUNT_TYPE_RELEASE);
}

static void
test_get_active_user (Test *test,
                      gconstpointer unused)
{
  gboolean result;
  g_autoptr (FromeAccount) account1 = NULL, account2;

  g_assert (frome_account_db_get_active_user (test->db) == NULL);

  /* Add an account and make it the active */
  result = frome_account_db_add_account (test->db, "mail1@test.com", "first1",
                                         "last1", "customer1", ACCOUNT_TYPE_DEVEL,
                                         TRUE, &test->error);
  g_assert_no_error (test->error);
  g_assert_true (result);

  account1 = frome_account_db_get_active_user (test->db);
  g_assert (account1 != NULL);
  g_assert_cmpstr (account1->email, ==, "mail1@test.com");

  /* Account is no longer active */
  result = frome_account_db_update_active_user (test->db, "mail1@test.com", FALSE, &test->error);
  g_assert_no_error (test->error);
  g_assert_true (result);

  g_assert (frome_account_db_get_active_user (test->db) == NULL);

  /* Add a second account and make it the new active */
  result = frome_account_db_add_account (test->db, "mail2@test.com", "first2",
                                         "last2", "customer2", ACCOUNT_TYPE_DEVEL,
                                         TRUE, &test->error);
  g_assert_no_error (test->error);
  g_assert_true (result);

  account2 = frome_account_db_get_active_user (test->db);
  g_assert (account2 != NULL);
  g_assert_cmpstr (account2->email, ==, "mail2@test.com");
}

int
main (int argc, char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/frome/account-db/add-account", Test, NULL,
              setup, test_add_account, teardown);
  g_test_add ("/frome/account-db/remove-account", Test, NULL,
              setup, test_remove_account, teardown);
  g_test_add ("/frome/account-db/update-active-user", Test, NULL,
              setup, test_update_active_user, teardown);
  g_test_add ("/frome/account-db/update-account-type", Test, NULL,
              setup, test_update_account_type, teardown);
  g_test_add ("/frome/account-db/get-active-user", Test, NULL,
              setup, test_get_active_user, teardown);

  return g_test_run ();
}
