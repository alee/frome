/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib/gstdio.h>

#include "src/app-db.h"

typedef struct
{
  FromeAppDB *db;

  gchar *dir;
  gchar *path;
  GError *error;
} Test;

static void
setup (Test *test, gconstpointer unused)
{
  test->dir = g_dir_make_tmp ("frome-test-app-db-XXXXXX", &test->error);
  g_assert_no_error (test->error);

  test->path = g_build_filename (test->dir, "app.db", NULL);

  test->db = frome_app_db_new (test->path);
  g_assert (FROME_IS_APP_DB (test->db));
}

static void
teardown (Test *test, gconstpointer unused)
{
  g_clear_object (&test->db);

  if (test->path != NULL)
    {
      g_unlink (test->path);
      g_clear_pointer (&test->path, g_free);
    }

  if (test->dir != NULL)
    {
      g_rmdir (test->dir);
      g_clear_pointer (&test->dir, g_free);
    }

  g_clear_error (&test->error);
}

static void
test_ensure (Test *test,
             gconstpointer unused)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;

  entry = frome_app_db_find_entry (test->db, "product-id1", &test->error);
  g_assert_no_error (test->error);
  g_assert (entry == NULL);

  entry = frome_app_db_ensure_entry (test->db, "product-id1", &test->error);
  g_assert_no_error (test->error);
  g_assert (FROME_IS_APP_DB_ENTRY (entry));
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, "product-id1");
  g_clear_object (&entry);

  entry = frome_app_db_ensure_entry (test->db, "product-id2", &test->error);
  g_assert_no_error (test->error);
  g_assert (FROME_IS_APP_DB_ENTRY (entry));
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, "product-id2");
  g_clear_object (&entry);

  /* Recreate the DB to test loading data */
  g_clear_object (&test->db);
  test->db = frome_app_db_new (test->path);

  entry = frome_app_db_find_entry (test->db, "product-id1", &test->error);
  g_assert_no_error (test->error);
  g_assert (FROME_IS_APP_DB_ENTRY (entry));
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, "product-id1");
  g_clear_object (&entry);

  entry = frome_app_db_find_entry (test->db, "product-id1", &test->error);
  g_assert_no_error (test->error);
  g_assert (FROME_IS_APP_DB_ENTRY (entry));
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, "product-id1");
  g_clear_object (&entry);
}

static void
test_update (Test *test,
             gconstpointer unused)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;

  entry = frome_app_db_ensure_entry (test->db, "product-id", &test->error);
  g_assert_no_error (test->error);
  g_assert (FROME_IS_APP_DB_ENTRY (entry));

  frome_app_db_entry_set_download_url (entry, "download-url");
  frome_app_db_entry_set_install_path (entry, "install-path");
  frome_app_db_entry_set_app_folder_name (entry, "app-folder-name");
  frome_app_db_entry_set_app_name (entry, "app-name");
  frome_app_db_entry_set_target_app_state (entry, APP_STATE_DOWNLOADING);
  frome_app_db_entry_set_store_app_state (entry, APP_STATE_PURCHASED);
  frome_app_db_entry_set_app_status (entry, APP_STATUS_EXTRACTION_IN_PROGRESS);
  frome_app_db_entry_set_app_schemas (entry, "app-schemas");
  frome_app_db_entry_set_config_file (entry, "config-file");
  frome_app_db_entry_set_version (entry, "version");
  frome_app_db_entry_set_app_users (entry, "app-users");
  frome_app_db_entry_set_rollback_version (entry, "rollback-version");
  frome_app_db_entry_set_process_type (entry, PROCESS_TYPE_APP_UPDATE);
  frome_app_db_entry_set_update_prod_id (entry, "update-prod-id");

  frome_app_db_entry_save (entry, &test->error);
  g_assert_no_error (test->error);

  /* Recreate the DB to test loading data */
  g_clear_object (&entry);
  g_clear_object (&test->db);
  test->db = frome_app_db_new (test->path);

  entry = frome_app_db_find_entry (test->db, "product-id", &test->error);
  g_assert_no_error (test->error);
  g_assert (FROME_IS_APP_DB_ENTRY (entry));
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, "product-id");
  g_assert_cmpstr (frome_app_db_entry_get_download_url (entry), ==, "download-url");
  g_assert_cmpstr (frome_app_db_entry_get_app_folder_name (entry), ==, "app-folder-name");
  g_assert_cmpstr (frome_app_db_entry_get_app_name (entry), ==, "app-name");
  g_assert_cmpuint (frome_app_db_entry_get_target_app_state (entry), ==, APP_STATE_DOWNLOADING);
  g_assert_cmpuint (frome_app_db_entry_get_store_app_state (entry), ==, APP_STATE_PURCHASED);
  g_assert_cmpuint (frome_app_db_entry_get_app_status (entry), ==, APP_STATUS_EXTRACTION_IN_PROGRESS);
  g_assert_cmpstr (frome_app_db_entry_get_app_schemas (entry), ==, "app-schemas");
  g_assert_cmpstr (frome_app_db_entry_get_config_file (entry), ==, "config-file");
  g_assert_cmpstr (frome_app_db_entry_get_version (entry), ==, "version");
  g_assert_cmpstr (frome_app_db_entry_get_app_users (entry), ==, "app-users");
  g_assert_cmpstr (frome_app_db_entry_get_rollback_version (entry), ==, "rollback-version");
  g_assert_cmpuint (frome_app_db_entry_get_process_type (entry), ==, PROCESS_TYPE_APP_UPDATE);
  g_assert_cmpstr (frome_app_db_entry_get_update_prod_id (entry), ==, "update-prod-id");
}

static void
test_get_all (Test *test,
              gconstpointer unused)
{
  FromeAppDBEntry *entry = NULL;
  g_autoptr (GPtrArray) items = NULL;

  items = frome_app_db_get_all_entries (test->db, &test->error);
  g_assert_no_error (test->error);
  g_assert (items != NULL);
  g_assert_cmpuint (items->len, ==, 0);
  g_clear_pointer (&items, g_ptr_array_unref);

  entry = frome_app_db_ensure_entry (test->db, "product-id1", &test->error);
  g_assert_no_error (test->error);
  g_assert (FROME_IS_APP_DB_ENTRY (entry));
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, "product-id1");
  g_clear_object (&entry);

  entry = frome_app_db_ensure_entry (test->db, "product-id2", &test->error);
  g_assert_no_error (test->error);
  g_assert (FROME_IS_APP_DB_ENTRY (entry));
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, "product-id2");
  g_clear_object (&entry);

  /* Recreate the DB to test loading data */
  g_clear_object (&entry);
  g_clear_object (&test->db);
  test->db = frome_app_db_new (test->path);

  items = frome_app_db_get_all_entries (test->db, &test->error);
  g_assert_no_error (test->error);
  g_assert (items != NULL);
  g_assert_cmpuint (items->len, ==, 2);

  entry = g_ptr_array_index (items, 0);
  g_assert (FROME_IS_APP_DB_ENTRY (entry));
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, "product-id1");

  entry = g_ptr_array_index (items, 1);
  g_assert (FROME_IS_APP_DB_ENTRY (entry));
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, "product-id2");
}

int
main (int argc, char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/frome/app-db/ensure", Test, NULL,
              setup, test_ensure, teardown);
  g_test_add ("/frome/app-db/update", Test, NULL,
              setup, test_update, teardown);
  g_test_add ("/frome/app-db/get-all", Test, NULL,
              setup, test_get_all, teardown);

  return g_test_run ();
}
