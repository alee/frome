/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <gio/gio.h>
#include <glib/gstdio.h>

#include "src/account-db.h"
#include "src/app-db.h"
#include "src/constants.h"
#include "src/errors.h"

#include "dbus/Frome.AccountMgr.h"
#include "dbus/Frome.DnldMgr.h"
#include "dbus/org.apertis.Frome.h"

#define MOCK_WEB_SERVER_BUS_NAME "org.apertis.Frome.MockWebStoreServer"
#define MOCK_WEB_SERVER_PATH "/org/apertis/Frome/MockWebStoreServer"
#define MOCK_WEB_SERVER_IFACE "org.apertis.Frome.MockWebStoreServer"

/* Need to stay synced with CUSTOMER_ID in tests/frome-mock-web-store-server */
#define CUSTOMER_ID "kHczotvae21ZuY6wkjkQ1jmljCMIioTihcpcTZfLjGLy0Rh2RrD9TSY0rhWSKy6GEXQfY7FntCHaElSzZaveROzLz3V1BGRlH_FKOcLFODaReGfdHnpLyJHEGZiTpDGSbismekMKxMINzaDWeI8J4EjHxmycWiBIxpGKBH-i5GIPaqJygJLMzxEOQqAkHC0ns8YTQvMEupLGxjHsaA5p-Mq97PwRmhD70QTyhQtV0naGXawZ1JCzvrl7AUiRR_lGd_OdzQW8dYWQAEjLF46mlbWbvyfSX0ohSXMo31hdc2lw2btDH1Aha6M4VNzDuOMPJI5gM5Rl2s1LjMg-oxG6uw,,"

#define PRODUCT_ID "666"
#define APP_NAME "org.apertis.Frome.TestAppBundle"
#define APP_VERSION_1 "1.0.0"
#define APP_VERSION_2 "2.0.0"
#define FILE_SIZE 1508

#define BUNDLE_INSTALL_PATH "/run/ribchester/general/app-bundles/"

typedef struct
{
  GTestDBus *dbus;
  GDBusConnection *conn;     /* owned */
  GDBusConnection *sys_conn; /* owned */
  guint wait_count;
  GMainLoop *loop;
  GError *error;
  guint watch_id;
  gboolean result;
  guint wait;

  gchar *dir;
  gchar *account_db_path;
  gchar *app_db_path;
  FromeAccountDB *account_db;
  FromeAppDB *app_db;

  FromeOrgApertisFrome *frome_proxy;
  FromeAccountMgr *account_mgr_proxy;
  FromeDnldMgr *dl_mgr_proxy;
  GDBusProxy *mock_proxy;
  gulong frome_proxy_prop_change_sig;
  gchar *http_server_url;

  GVariant *create_account_info;
  gboolean create_account_status;
  gchar *create_account_error_msg;
  GVariant *get_accounts_result;

  gboolean got_download_progress_sig;
  gchar *progress_app_state;

  gboolean got_download_status_sig;
  gchar *status_app_state;
  gchar *download_status;
  gint download_status_result_code;
} Test;

static void
setup (Test *test, gconstpointer unused)
{
  const gchar *addr;

  test->error = NULL;

  /* Register D-Bus errors */
  frome_account_manager_error_quark ();
  frome_download_manager_error_quark ();

  if (g_getenv ("FROME_TESTS_UNINSTALLED") != NULL)
    {
      /* Test is not installed */
      test->dbus = g_test_dbus_new (G_TEST_DBUS_NONE);
      g_test_dbus_add_service_dir (test->dbus, TEST_SERVICES_DIR);
      g_test_dbus_up (test->dbus);
      addr = g_test_dbus_get_bus_address (test->dbus);
      g_assert (addr != NULL);

      test->conn = g_dbus_connection_new_for_address_sync (addr,
                                                           G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
                                                               G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION,
                                                           NULL, NULL, &test->error);
    }
  else
    {
      test->conn = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &test->error);
    }

  g_assert_no_error (test->error);

  test->sys_conn = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &test->error);
  g_assert_no_error (test->error);

  test->loop = g_main_loop_new (NULL, FALSE);

  /* Use a temp dir to store the DBs */
  test->dir = g_dir_make_tmp ("frome-test-service-XXXXXX", &test->error);
  g_assert_no_error (test->error);
  test->account_db_path = g_build_filename (test->dir, "account-db.ini", NULL);
  test->app_db_path = g_build_filename (test->dir, "applications.db", NULL);

  /* Create DB instances on the same path to check the DB in tests */
  test->account_db = frome_account_db_new (test->account_db_path);
  test->app_db = frome_app_db_new (test->app_db_path);
}

static void
dbus_prop_changed_cb (GDBusProxy *proxy,
                      GVariant *changed_properties,
                      GStrv invalidated_properties,
                      Test *test)
{
  g_main_loop_quit (test->loop);
}

static void
change_web_store_url (Test *test,
                      const gchar *url)
{
  if (g_strcmp0 (frome_org_apertis_frome_get_web_store_url (test->frome_proxy), url) == 0)
    return;

  if (test->frome_proxy_prop_change_sig == 0)
    test->frome_proxy_prop_change_sig = g_signal_connect (test->frome_proxy,
                                                          "g-properties-changed",
                                                          G_CALLBACK (dbus_prop_changed_cb), test);

  frome_org_apertis_frome_set_web_store_url (test->frome_proxy, url);
  g_main_loop_run (test->loop);
}

static void
change_account_db_path (Test *test,
                        const gchar *path)
{
  if (g_strcmp0 (frome_org_apertis_frome_get_account_dbpath (test->frome_proxy), path) == 0)
    return;

  if (test->frome_proxy_prop_change_sig == 0)
    test->frome_proxy_prop_change_sig = g_signal_connect (test->frome_proxy,
                                                          "g-properties-changed",
                                                          G_CALLBACK (dbus_prop_changed_cb), test);

  frome_org_apertis_frome_set_account_dbpath (test->frome_proxy, path);
  g_main_loop_run (test->loop);
}

static void
change_app_db_path (Test *test,
                    const gchar *path)
{
  if (g_strcmp0 (frome_org_apertis_frome_get_application_dbpath (test->frome_proxy), path) == 0)
    return;

  if (test->frome_proxy_prop_change_sig == 0)
    test->frome_proxy_prop_change_sig = g_signal_connect (test->frome_proxy,
                                                          "g-properties-changed",
                                                          G_CALLBACK (dbus_prop_changed_cb), test);

  frome_org_apertis_frome_set_application_dbpath (test->frome_proxy, path);
  g_main_loop_run (test->loop);
  g_assert_cmpstr (frome_org_apertis_frome_get_application_dbpath (test->frome_proxy), ==, path);
}

static void
teardown (Test *test, gconstpointer unused)
{
  if (test->frome_proxy != NULL)
    {
      /* Restore the default settings to not break the real Frome service
       * when running installed tests */
      change_web_store_url (test, DEFAULT_WEB_SERVER_URL);
      change_account_db_path (test, "");
      change_app_db_path (test, "");
      g_clear_object (&test->frome_proxy);
    }

  g_clear_pointer (&test->loop, g_main_loop_unref);
  g_clear_pointer (&test->error, g_error_free);

  if (test->watch_id != 0)
    {
      g_bus_unwatch_name (test->watch_id);
      test->watch_id = 0;
    }

  g_clear_object (&test->conn);
  g_clear_object (&test->sys_conn);
  g_clear_object (&test->account_mgr_proxy);
  g_clear_object (&test->dl_mgr_proxy);
  g_clear_object (&test->mock_proxy);

  g_clear_pointer (&test->create_account_info, g_variant_unref);
  g_clear_pointer (&test->create_account_error_msg, g_free);
  g_clear_pointer (&test->get_accounts_result, g_variant_unref);

  if (test->dbus != NULL)
    {
      g_test_dbus_down (test->dbus);
      g_clear_object (&test->dbus);
    }

  g_clear_object (&test->account_db);
  g_clear_object (&test->app_db);

  if (test->account_db_path != NULL)
    {
      g_unlink (test->account_db_path);
      g_clear_pointer (&test->account_db_path, g_free);
    }

  if (test->dir != NULL)
    {
      g_rmdir (test->dir);
      g_clear_pointer (&test->dir, g_free);
    }

  g_clear_pointer (&test->http_server_url, g_free);

  g_clear_pointer (&test->progress_app_state, g_free);
  g_clear_pointer (&test->status_app_state, g_free);
  g_clear_pointer (&test->download_status, g_free);
}

static void
frome_bus_name_appeared_cb (GDBusConnection *connection,
                            const gchar *name,
                            const gchar *name_owner,
                            gpointer user_data)
{
  Test *test = user_data;

  g_main_loop_quit (test->loop);
}

static void
frome_bus_name_vanished_cb (GDBusConnection *connection,
                            const gchar *name,
                            gpointer user_data)
{
  g_critical ("Bus name %s vanished", name);
}

/* Test activating the service using FROME_BUS_NAME */
static void
test_activation_full_name (Test *test,
                           gconstpointer unused)
{
  test->watch_id = g_bus_watch_name_on_connection (test->conn, FROME_BUS_NAME,
                                                   G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                                                   frome_bus_name_appeared_cb,
                                                   frome_bus_name_vanished_cb,
                                                   test, NULL);

  g_main_loop_run (test->loop);
}

static void
start_account_manger (Test *test)
{
  test->watch_id = g_bus_watch_name_on_connection (test->conn, FROME_ACCOUNT_MANAGER_BUS_NAME,
                                                   G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                                                   frome_bus_name_appeared_cb,
                                                   frome_bus_name_vanished_cb,
                                                   test, NULL);

  g_main_loop_run (test->loop);
}

/* Test activating the service using FROME_ACCOUNT_MANAGER_BUS_NAME */
static void
test_activation_account_manager (Test *test,
                                 gconstpointer unused)
{
  start_account_manger (test);
}

static void
start_download_manager (Test *test)
{
  test->watch_id = g_bus_watch_name_on_connection (test->conn, FROME_DOWNLOAD_MANAGER_BUS_NAME,
                                                   G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                                                   frome_bus_name_appeared_cb,
                                                   frome_bus_name_vanished_cb,
                                                   test, NULL);

  g_main_loop_run (test->loop);
}

/* Test activating the service using FROME_DOWNLOAD_MANAGER_BUS_NAME */
static void
test_activation_download_manager (Test *test,
                                  gconstpointer unused)
{
  start_download_manager (test);
}

static void
frome_proxy_cb (GObject *source,
                GAsyncResult *result,
                gpointer user_data)
{
  Test *test = user_data;

  test->frome_proxy = frome_org_apertis_frome_proxy_new_finish (result, &test->error);
  g_main_loop_quit (test->loop);
}

static void
create_frome_proxy (Test *test)
{
  g_assert (test->frome_proxy == NULL);

  frome_org_apertis_frome_proxy_new (test->conn, G_DBUS_PROXY_FLAGS_NONE,
                                     FROME_BUS_NAME,
                                     FROME_PATH, NULL,
                                     frome_proxy_cb, test);

  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert (FROME_IS_ORG_APERTIS_FROME_PROXY (test->frome_proxy));
}

static void
account_mgr_proxy_cb (GObject *source,
                      GAsyncResult *result,
                      gpointer user_data)
{
  Test *test = user_data;

  test->account_mgr_proxy = frome_account_mgr_proxy_new_finish (result, &test->error);
  g_main_loop_quit (test->loop);
}

static void
create_account_manager_proxy (Test *test)
{
  g_assert (test->account_mgr_proxy == NULL);

  frome_account_mgr_proxy_new (test->conn, G_DBUS_PROXY_FLAGS_NONE,
                               FROME_ACCOUNT_MANAGER_BUS_NAME,
                               FROME_ACCOUNT_MANAGER_PATH, NULL,
                               account_mgr_proxy_cb, test);

  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert (FROME_IS_ACCOUNT_MGR_PROXY (test->account_mgr_proxy));
}

static void
download_mgr_proxy_cb (GObject *source,
                       GAsyncResult *result,
                       gpointer user_data)
{
  Test *test = user_data;

  test->dl_mgr_proxy = frome_dnld_mgr_proxy_new_finish (result, &test->error);
  g_main_loop_quit (test->loop);
}

static void
create_download_manager_proxy (Test *test)
{
  g_assert (test->dl_mgr_proxy == NULL);

  frome_dnld_mgr_proxy_new (test->conn, G_DBUS_PROXY_FLAGS_NONE,
                            FROME_DOWNLOAD_MANAGER_BUS_NAME,
                            FROME_DOWNLOAD_MANAGER_PATH, NULL,
                            download_mgr_proxy_cb, test);

  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert (FROME_IS_DNLD_MGR_PROXY (test->dl_mgr_proxy));
}

static void
mock_proxy_cb (GObject *source,
               GAsyncResult *result,
               gpointer user_data)
{
  Test *test = user_data;

  test->mock_proxy = g_dbus_proxy_new_finish (result, &test->error);
  g_main_loop_quit (test->loop);
}

/* Invoke the mock server, create a proxy and configure Frome to use it */
static void
start_mock_web_server (Test *test)
{
  guint id;
  g_autoptr (GVariant) v = NULL;

  /* Start the mock server */
  id = g_bus_watch_name_on_connection (test->conn, MOCK_WEB_SERVER_BUS_NAME,
                                       G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                                       frome_bus_name_appeared_cb,
                                       frome_bus_name_vanished_cb,
                                       test, NULL);

  g_main_loop_run (test->loop);
  g_bus_unwatch_name (id);

  /* Create a proxy on the mock server */
  g_dbus_proxy_new (test->conn, G_DBUS_PROXY_FLAGS_NONE, NULL,
                    MOCK_WEB_SERVER_BUS_NAME, MOCK_WEB_SERVER_PATH,
                    MOCK_WEB_SERVER_IFACE, NULL, mock_proxy_cb, test);

  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);

  /* Retrieve the web server port */
  v = g_dbus_proxy_get_cached_property (test->mock_proxy, "Port");
  g_assert (v != NULL);

  g_assert (test->http_server_url == NULL);
  test->http_server_url = g_strdup_printf ("http://127.0.0.1:%u", g_variant_get_uint16 (v));
  change_web_store_url (test, test->http_server_url);
}

/* Invoke Frome and the mock server. Create proxies and configure Frome to
 * use the mock server. */
static void
start_and_configure_frome (Test *test)
{
  start_account_manger (test);
  create_frome_proxy (test);
  create_account_manager_proxy (test);
  create_download_manager_proxy (test);
  start_mock_web_server (test);
  change_account_db_path (test, test->account_db_path);
  change_app_db_path (test, test->app_db_path);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->account_db), ==, 0);
  g_assert_cmpuint (g_utf8_strlen (frome_org_apertis_frome_get_device_id (test->frome_proxy), -1), >, 0);
}

static void
create_account_cb (GObject *proxy,
                   GAsyncResult *result,
                   gpointer user_data)
{
  Test *test = user_data;

  test->result = frome_account_mgr_call_create_account_finish (FROME_ACCOUNT_MGR (proxy),
                                                               result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static void
account_status_cb (FromeAccountMgr *proxy,
                   GVariant *info,
                   gboolean status,
                   const gchar *error_message,
                   Test *test)
{
  g_clear_pointer (&test->create_account_info, g_variant_unref);
  g_clear_pointer (&test->create_account_error_msg, g_free);

  test->create_account_info = g_variant_ref (info);
  test->create_account_status = status;
  test->create_account_error_msg = g_strdup (error_message);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static void
create_account_and_wait_account_status (Test *test,
                                        const gchar *email)
{
  GVariantBuilder builder;

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{ss}"));
  g_variant_builder_add (&builder, "{ss}", "firstname", "MyFirstName");
  g_variant_builder_add (&builder, "{ss}", "lastname", "MyLastName");
  g_variant_builder_add (&builder, "{ss}", "e_mail", email);
  g_variant_builder_add (&builder, "{ss}", "order_newsletter", "NO");
  g_variant_builder_add (&builder, "{ss}", "telephone", "123456789");
  g_variant_builder_add (&builder, "{ss}", "streetname_housenumber", "my street");
  g_variant_builder_add (&builder, "{ss}", "city", "my-city");
  g_variant_builder_add (&builder, "{ss}", "postalcode", "666");
  g_variant_builder_add (&builder, "{ss}", "state", "my-state");
  g_variant_builder_add (&builder, "{ss}", "country", "my-country");
  g_variant_builder_add (&builder, "{ss}", "password", "my-password");

  g_signal_connect (test->account_mgr_proxy, "account-status",
                    G_CALLBACK (account_status_cb), test);

  frome_account_mgr_call_create_account (test->account_mgr_proxy,
                                         g_variant_builder_end (&builder), NULL,
                                         create_account_cb, test);

  test->wait = 2; /* create_account_cb and account_status_cb */
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert_true (test->result);
}

/* Test Frome.AccountMgr.CreateAccount() */
static void
test_account_manager_create_account (Test *test,
                                     gconstpointer unused)
{
  const gchar *str;
  g_autoptr (GPtrArray) accounts = NULL;
  FromeAccount *account;

  start_and_configure_frome (test);

  create_account_and_wait_account_status (test, "my@email.com");

  g_assert (test->create_account_info != NULL);
  g_assert (g_variant_lookup (test->create_account_info, "Firstname", "&s", &str));
  g_assert_cmpstr (str, ==, "MyFirstName");
  g_assert (g_variant_lookup (test->create_account_info, "Lastname", "&s", &str));
  g_assert_cmpstr (str, ==, "MyLastName");
  g_assert (g_variant_lookup (test->create_account_info, "Emailid", "&s", &str));
  g_assert_cmpstr (str, ==, "my@email.com");
  g_assert_cmpstr (test->create_account_error_msg, ==, "NULL");
  g_assert_true (test->create_account_status);
  g_assert (g_variant_lookup (test->create_account_info, "CustomerID", "&s", &str));
  g_assert_cmpstr (str, ==, CUSTOMER_ID);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->account_db), ==, 1);
  accounts = frome_account_db_get_accounts (test->account_db);
  g_assert_cmpuint (accounts->len, ==, 1);
  account = g_ptr_array_index (accounts, 0);

  g_assert_cmpstr (account->email, ==, "my@email.com");
  g_assert_cmpstr (account->first_name, ==, "MyFirstName");
  g_assert_cmpstr (account->last_name, ==, "MyLastName");
  g_assert (account->customer_id != NULL);
  g_assert_cmpuint (account->type, ==, ACCOUNT_TYPE_BOTH); /* both is the default account type */
  g_assert_true (account->active_user);
}

/* Frome.AccountMgr.CreateAccount() call failing */
static void
test_account_manager_create_account_call_fail (Test *test,
                                               gconstpointer unused)
{
  GVariantBuilder builder;

  start_and_configure_frome (test);

  /* No parameter */
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{ss}"));

  frome_account_mgr_call_create_account (test->account_mgr_proxy,
                                         g_variant_builder_end (&builder), NULL,
                                         create_account_cb, test);

  test->wait = 1;
  g_main_loop_run (test->loop);
  g_assert_error (test->error, FROME_ACCOUNT_MANAGER_ERROR, FROME_ACCOUNT_MANAGER_ERROR_MISSING_DETAILS);
  g_assert_false (test->result);
  g_clear_error (&test->error);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->account_db), ==, 0);

  /* Wrong boolean parameter */
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{ss}"));
  g_variant_builder_add (&builder, "{ss}", "order_newsletter", "badger");

  frome_account_mgr_call_create_account (test->account_mgr_proxy,
                                         g_variant_builder_end (&builder), NULL,
                                         create_account_cb, test);

  test->wait = 1;
  g_main_loop_run (test->loop);
  g_assert_error (test->error, FROME_ACCOUNT_MANAGER_ERROR, FROME_ACCOUNT_MANAGER_ERROR_INVALID_ARGS);
  g_assert_false (test->result);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->account_db), ==, 0);
}

/* CreateAccount() call succeed but server isn't happy so error is reported from
 * the AccountStatus signal */
static void
test_account_manager_create_account_sig_fail (Test *test,
                                              gconstpointer unused)
{
  const gchar *str;

  start_and_configure_frome (test);

  /* Server will reject the call when using this first email */
  create_account_and_wait_account_status (test, "FAIL@email.com");

  g_assert (test->create_account_info != NULL);
  g_assert (g_variant_lookup (test->create_account_info, "Emailid", "&s", &str));
  g_assert_cmpstr (str, ==, "FAIL@email.com");
  g_assert_cmpstr (test->create_account_error_msg, !=, "");
  g_assert_false (test->create_account_status);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->account_db), ==, 0);
}

static void
delete_account_cb (GObject *proxy,
                   GAsyncResult *result,
                   gpointer user_data)
{
  Test *test = user_data;

  test->result = frome_account_mgr_call_delete_account_finish (FROME_ACCOUNT_MGR (proxy),
                                                               result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

/* Test Frome.AccountMgr.DeleteAccount() */
static void
test_account_manager_delete_account (Test *test,
                                     gconstpointer unused)
{
  g_autoptr (FromeAccount) account1 = NULL, account2 = NULL;

  start_and_configure_frome (test);

  /* Add two accounts */
  create_account_and_wait_account_status (test, "my@email.com");
  create_account_and_wait_account_status (test, "my2@email.com");

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->account_db), ==, 2);

  /* First added account is the active one */
  account1 = frome_account_db_get_account (test->account_db, "my@email.com");
  g_assert (account1 != NULL);
  g_assert_true (account1->active_user);
  g_clear_pointer (&account1, frome_account_unref);

  account2 = frome_account_db_get_account (test->account_db, "my2@email.com");
  g_assert (account2 != NULL);
  g_assert_false (account2->active_user);
  g_clear_pointer (&account2, frome_account_unref);

  /* Delete the first account */
  frome_account_mgr_call_delete_account (test->account_mgr_proxy, "my@email.com",
                                         NULL, delete_account_cb, test);
  test->wait = 1;
  g_main_loop_run (test->loop);

  g_assert_no_error (test->error);
  g_assert_true (test->result);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->account_db), ==, 1);

  /* The second account is now the active one */
  account1 = frome_account_db_get_account (test->account_db, "my@email.com");
  g_assert (account1 == NULL);

  account2 = frome_account_db_get_account (test->account_db, "my2@email.com");
  g_assert (account2 != NULL);
  g_assert_true (account2->active_user);
}

/* Frome.AccountMgr.DeleteAccount() failing */
static void
test_account_manager_delete_account_fail (Test *test,
                                          gconstpointer unused)
{
  start_and_configure_frome (test);

  /* Add an account so the DB is created */
  create_account_and_wait_account_status (test, "badger@email.com");

  /* Try deleting an unexisting account */
  frome_account_mgr_call_delete_account (test->account_mgr_proxy, "my@email.com",
                                         NULL, delete_account_cb, test);
  test->wait = 1;
  g_main_loop_run (test->loop);

  g_assert_error (test->error, FROME_ACCOUNT_MANAGER_ERROR,
                  FROME_ACCOUNT_MANAGER_ERROR_ACCOUNT_NOT_FOUND);
  g_assert_false (test->result);
}

static void
get_accounts_cb (GObject *proxy,
                 GAsyncResult *result,
                 gpointer user_data)
{
  Test *test = user_data;

  g_clear_pointer (&test->get_accounts_result, g_variant_unref);

  test->result = frome_account_mgr_call_get_accounts_finish (FROME_ACCOUNT_MGR (proxy),
                                                             &test->get_accounts_result,
                                                             result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

/* Test Frome.AccountMgr.GetAccounts() */
static void
test_account_manager_get_accounts (Test *test,
                                   gconstpointer unused)
{
  const gchar *str;
  g_autoptr (GVariant) v = NULL;

  start_and_configure_frome (test);

  frome_account_mgr_call_get_accounts (test->account_mgr_proxy, NULL, get_accounts_cb, test);
  test->wait = 1;
  g_main_loop_run (test->loop);

  g_assert_no_error (test->error);
  g_assert_true (test->result);

  g_assert (test->get_accounts_result != NULL);
  g_assert_cmpuint (g_variant_n_children (test->get_accounts_result), ==, 0);

  create_account_and_wait_account_status (test, "my@email.com");

  frome_account_mgr_call_get_accounts (test->account_mgr_proxy, NULL, get_accounts_cb, test);
  test->wait = 1;
  g_main_loop_run (test->loop);

  g_assert_no_error (test->error);
  g_assert_true (test->result);

  g_assert (test->get_accounts_result != NULL);
  g_assert_cmpuint (g_variant_n_children (test->get_accounts_result), ==, 1);

  v = g_variant_get_child_value (test->get_accounts_result, 0);
  g_assert (g_variant_lookup (v, "Firstname", "&s", &str));
  g_assert_cmpstr (str, ==, "MyFirstName");
  g_assert (g_variant_lookup (v, "Lastname", "&s", &str));
  g_assert_cmpstr (str, ==, "MyLastName");
  g_assert (g_variant_lookup (v, "Emailid", "&s", &str));
  g_assert_cmpstr (str, ==, "my@email.com");
  g_assert (g_variant_lookup (v, "ActiveUser", "&s", &str));
  g_assert_cmpstr (str, ==, "true");
  g_assert (g_variant_lookup (v, "Distribution", "&s", &str));
  g_assert_cmpstr (str, ==, "both");
  g_assert (g_variant_lookup (v, "CustomerID", "&s", &str));
  g_assert_cmpstr (str, ==, CUSTOMER_ID);
}

static void
set_default_account_cb (GObject *proxy,
                        GAsyncResult *result,
                        gpointer user_data)
{
  Test *test = user_data;

  test->result = frome_account_mgr_call_set_default_account_finish (FROME_ACCOUNT_MGR (proxy),
                                                                    result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

/* Test Frome.AccountMgr.SetDefaultAccount() */
static void
test_account_manager_set_default_account (Test *test,
                                          gconstpointer unused)
{
  g_autoptr (FromeAccount) account1 = NULL, account2 = NULL;

  start_and_configure_frome (test);

  /* Add two accounts */
  create_account_and_wait_account_status (test, "my@email.com");
  create_account_and_wait_account_status (test, "my2@email.com");

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->account_db), ==, 2);

  /* First added account is the active one */
  account1 = frome_account_db_get_account (test->account_db, "my@email.com");
  g_assert (account1 != NULL);
  g_assert_true (account1->active_user);
  g_clear_pointer (&account1, frome_account_unref);

  account2 = frome_account_db_get_account (test->account_db, "my2@email.com");
  g_assert (account2 != NULL);
  g_assert_false (account2->active_user);
  g_clear_pointer (&account2, frome_account_unref);

  /* Make the second the active one */
  frome_account_mgr_call_set_default_account (test->account_mgr_proxy, "my2@email.com",
                                              NULL, set_default_account_cb, test);
  test->wait = 1;
  g_main_loop_run (test->loop);

  g_assert_no_error (test->error);
  g_assert_true (test->result);

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->account_db), ==, 2);

  /* The second account is now the active one */
  account1 = frome_account_db_get_account (test->account_db, "my@email.com");
  g_assert (account1 != NULL);
  g_assert_false (account1->active_user);

  account2 = frome_account_db_get_account (test->account_db, "my2@email.com");
  g_assert (account2 != NULL);
  g_assert_true (account2->active_user);
}

/* Frome.AccountMgr.SetDefaultAccount() failing */
static void
test_account_manager_set_default_account_fail (Test *test,
                                               gconstpointer unused)
{
  start_and_configure_frome (test);

  /* Add an account so the DB is created */
  create_account_and_wait_account_status (test, "badger@email.com");

  /* The account doesn't exist */
  frome_account_mgr_call_set_default_account (test->account_mgr_proxy, "my@email.com",
                                              NULL, set_default_account_cb, test);
  test->wait = 1;
  g_main_loop_run (test->loop);

  g_assert_error (test->error, FROME_ACCOUNT_MANAGER_ERROR,
                  FROME_ACCOUNT_MANAGER_ERROR_ACCOUNT_NOT_FOUND);
  g_assert_false (test->result);
}

static void
set_account_type_cb (GObject *proxy,
                     GAsyncResult *result,
                     gpointer user_data)
{
  Test *test = user_data;

  test->result = frome_account_mgr_call_set_account_type_finish (FROME_ACCOUNT_MGR (proxy),
                                                                 result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

/* Test Frome.AccountMgr.SetAccountType() */
static void
test_account_manager_set_account_type (Test *test,
                                       gconstpointer unused)
{
  g_autoptr (FromeAccount) account = NULL;

  start_and_configure_frome (test);

  create_account_and_wait_account_status (test, "my@email.com");

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->account_db), ==, 1);

  account = frome_account_db_get_account (test->account_db, "my@email.com");
  g_assert (account != NULL);
  /* Account has type 'both' by default */
  g_assert_cmpuint (account->type, ==, ACCOUNT_TYPE_BOTH);
  g_clear_pointer (&account, frome_account_unref);

  /* Change account type to 'devel' */
  frome_account_mgr_call_set_account_type (test->account_mgr_proxy, "my@email.com",
                                           "devel", NULL, set_account_type_cb,
                                           test);
  test->wait = 1;
  g_main_loop_run (test->loop);

  g_assert_no_error (test->error);
  g_assert_true (test->result);

/*
 * FIXME AT NEXT API BREAK: The DB is actually updated once Frome get the reply from the server.
 * But the current D-Bus API doesn't notify this on the bus so we don't have
 * any way to know when we can check this. */
#if 0
  account = frome_account_db_get_account (test->account_db, "my@email.com");
  g_assert (account != NULL);
  g_assert_cmpuint (account->type, ==, ACCOUNT_TYPE_DEVEL);
  g_clear_pointer (&account, frome_account_unref);
#endif

  /* Change account type to 'release' */
  frome_account_mgr_call_set_account_type (test->account_mgr_proxy, "my@email.com",
                                           "release", NULL, set_account_type_cb,
                                           test);
  test->wait = 1;
  g_main_loop_run (test->loop);

  g_assert_no_error (test->error);
  g_assert_true (test->result);

#if 0
  account = frome_account_db_get_account (test->account_db, "my@email.com");
  g_assert (account != NULL);
  g_assert_cmpuint (account->type, ==, ACCOUNT_TYPE_RELEASE);
  g_clear_pointer (&account, frome_account_unref);
#endif
}

/* Frome.AccountMgr.SetAccountType() failing */
static void
test_account_manager_set_account_type_fail (Test *test,
                                            gconstpointer unused)
{
  start_and_configure_frome (test);

  /* Account doesn't exist */
  frome_account_mgr_call_set_account_type (test->account_mgr_proxy, "my@email.com",
                                           "devel", NULL, set_account_type_cb,
                                           test);
  test->wait = 1;
  g_main_loop_run (test->loop);
  g_assert_error (test->error, FROME_ACCOUNT_MANAGER_ERROR,
                  FROME_ACCOUNT_MANAGER_ERROR_ACCOUNT_NOT_FOUND);
  g_assert_false (test->result);
  g_clear_error (&test->error);

  create_account_and_wait_account_status (test, "my@email.com");

  g_assert_cmpuint (frome_account_db_get_n_accounts (test->account_db), ==, 1);

  /* Invalid account type */
  frome_account_mgr_call_set_account_type (test->account_mgr_proxy, "my@email.com",
                                           "badger", NULL, set_account_type_cb,
                                           test);

  test->wait = 1;
  g_main_loop_run (test->loop);
  g_assert_error (test->error, FROME_ACCOUNT_MANAGER_ERROR,
                  FROME_ACCOUNT_MANAGER_ERROR_INVALID_ACCOUNT_TYPE);
  g_assert_false (test->result);
}

static void
start_appstore_download_cb (GObject *proxy,
                            GAsyncResult *result,
                            gpointer user_data)
{
  Test *test = user_data;

  test->result = frome_dnld_mgr_call_start_appstore_download_finish (FROME_DNLD_MGR (proxy),
                                                                     result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static gchar *
build_bundle_file_name (const gchar *version)
{
  return g_strdup_printf ("%s-%s.bundle", APP_NAME, version);
}

static gchar *
build_download_url (Test *test,
                    const gchar *version)
{
  g_autofree gchar *file_name = build_bundle_file_name (version);

  return g_strdup_printf ("%s/%s", test->http_server_url, file_name);
}

static void
download_progress_cb (FromeDnldMgr *proxy,
                      const gchar *application_name,
                      const gchar *product_id,
                      const gchar *app_state,
                      gdouble progress_percent,
                      Test *test)
{
  test->got_download_progress_sig = TRUE;

  g_assert_cmpstr (application_name, ==, APP_NAME);
  g_assert_cmpstr (product_id, ==, PRODUCT_ID);
  g_assert_cmpfloat (progress_percent, >=, 0.0);
  g_assert_cmpfloat (progress_percent, <=, 1.0);

  g_clear_pointer (&test->progress_app_state, g_free);
  test->progress_app_state = g_strdup (app_state);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static void
download_status_cb (FromeDnldMgr *proxy,
                    const gchar *application_name,
                    const gchar *product_id,
                    const gchar *app_state,
                    const gchar *status,
                    gint result_code,
                    Test *test)
{
  test->got_download_status_sig = TRUE;

  g_assert_cmpstr (application_name, ==, APP_NAME);
  g_assert_cmpstr (product_id, ==, PRODUCT_ID);

  g_clear_pointer (&test->status_app_state, g_free);
  test->status_app_state = g_strdup (app_state);
  g_clear_pointer (&test->download_status, g_free);
  test->download_status = g_strdup (status);
  test->download_status_result_code = result_code;

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

typedef enum {
  STATE_WAIT_DOWNLOAD_PROGRESS_INSTALLING,
  STATE_WAIT_DOWNLOAD_STATUS_INSTALLED,
  STATE_DONE,
} InstallationStep;

static gboolean
bundle_is_installed (const gchar *version)
{
  g_autofree gchar *path = NULL;
  g_autoptr (GFile) f = NULL;

  if (version == NULL)
    path = g_build_filename (BUNDLE_INSTALL_PATH, APP_NAME, NULL);
  else
    {
      g_autofree gchar *version_dir = g_strdup_printf ("version-%s", version);

      path = g_build_filename (BUNDLE_INSTALL_PATH, APP_NAME, version_dir, NULL);
    }

  f = g_file_new_for_path (path);

  return g_file_query_exists (f, NULL);
}

static void
remove_bundle (Test *test)
{
  g_autoptr (GVariant) result = NULL;

  if (!bundle_is_installed (NULL))
    return;

  result = g_dbus_connection_call_sync (test->sys_conn,
                                        "org.apertis.Ribchester",
                                        "/org/apertis/Ribchester/AppStore",
                                        "org.apertis.Ribchester.AppStore",
                                        "RemoveApp",
                                        g_variant_new ("(s)", APP_NAME),
                                        NULL,
                                        G_DBUS_CALL_FLAGS_NONE,
                                        -1,
                                        NULL,
                                        NULL);

  g_assert_false (bundle_is_installed (NULL));
}

static void
install_bundle (Test *test)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;
  g_autofree gchar *url = NULL;
  InstallationStep step;
  g_autofree gchar *file_name = NULL;

  start_and_configure_frome (test);
  create_account_and_wait_account_status (test, "my@email.com");

  /* ensure the bundle isn't installed yet */
  remove_bundle (test);

  entry = frome_app_db_find_entry (test->app_db, PRODUCT_ID, &test->error);
  g_assert_no_error (test->error);
  g_assert (entry == NULL);

  url = build_download_url (test, APP_VERSION_1);

  g_signal_connect (test->dl_mgr_proxy, "appstore-download-progress",
                    G_CALLBACK (download_progress_cb), test);
  g_signal_connect (test->dl_mgr_proxy, "appstore-download-status",
                    G_CALLBACK (download_status_cb), test);

  frome_dnld_mgr_call_start_appstore_download (test->dl_mgr_proxy, APP_NAME,
                                               url, PRODUCT_ID, APP_VERSION_1,
                                               FILE_SIZE, NULL, start_appstore_download_cb, test);

  test->wait = 1;
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert_true (test->result);

  entry = frome_app_db_find_entry (test->app_db, PRODUCT_ID, &test->error);
  g_assert_no_error (test->error);
  g_assert (entry != NULL);
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, PRODUCT_ID);
  g_assert_cmpstr (frome_app_db_entry_get_download_url (entry), ==, url);
  file_name = build_bundle_file_name (APP_VERSION_1);
  g_assert (g_str_has_suffix (frome_app_db_entry_get_install_path (entry), file_name));
  g_assert_cmpstr (frome_app_db_entry_get_app_name (entry), ==, APP_NAME);
  g_assert_cmpuint (frome_app_db_entry_get_target_app_state (entry), ==, APP_STATE_DOWNLOAD_READY);
  /* Store state is either initial or download-ready, depending if it has already been updated or not */
  g_assert (frome_app_db_entry_get_store_app_state (entry) == APP_STATE_INITIAL ||
            frome_app_db_entry_get_store_app_state (entry) == APP_STATE_DOWNLOAD_READY);
  g_assert_cmpuint (frome_app_db_entry_get_app_status (entry), ==, APP_STATUS_DOWNLOAD_STARTED);
  g_assert_cmpstr (frome_app_db_entry_get_version (entry), ==, APP_VERSION_1);
  g_assert_cmpuint (frome_app_db_entry_get_process_type (entry), ==, PROCESS_TYPE_APP_INSTALL);
  g_clear_object (&entry);

  step = STATE_WAIT_DOWNLOAD_PROGRESS_INSTALLING;
  while (step != STATE_DONE)
    {
      test->wait = 1;
      g_main_loop_run (test->loop);
      g_assert_no_error (test->error);
      g_assert_true (test->result);

      if (step == STATE_WAIT_DOWNLOAD_PROGRESS_INSTALLING)
        {
          g_assert_true (test->got_download_progress_sig);
          test->got_download_progress_sig = FALSE;

          if (g_strcmp0 (test->progress_app_state, FROME_APPSTORE_SIG_STATE_LOADING) == 0)
            /* We can receive a bunch of 'loading' signals informing us about download progress */
            continue;

          g_assert_cmpstr (test->progress_app_state, ==, FROME_APPSTORE_SIG_STATE_INSTALLING);
          /* App is installing wait for it to be installed */
          step = STATE_WAIT_DOWNLOAD_STATUS_INSTALLED;

          if (!test->got_download_status_sig)
            continue;

          /* We already received the next signal so no need to wait */
        }

      if (step == STATE_WAIT_DOWNLOAD_STATUS_INSTALLED)
        {
          g_assert_true (test->got_download_status_sig);
          test->got_download_status_sig = FALSE;

          g_assert_cmpstr (test->status_app_state, ==, FROME_APPSTORE_SIG_STATE_INSTALLED);
          g_assert_cmpstr (test->download_status, ==, FROME_APPSTORE_DOWNLOAD_STATUS_SUCCESS);
          g_assert_cmpint (test->download_status_result_code, ==, FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_NO_ERROR);

          step = STATE_DONE;
        }
      else
        g_assert_not_reached ();
    }

  g_assert_true (bundle_is_installed (APP_VERSION_1));

  entry = frome_app_db_find_entry (test->app_db, PRODUCT_ID, &test->error);
  g_assert_no_error (test->error);
  g_assert (entry != NULL);
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, PRODUCT_ID);
  g_assert_cmpuint (frome_app_db_entry_get_target_app_state (entry), ==, APP_STATE_INSTALLED);
  g_assert_cmpuint (frome_app_db_entry_get_app_status (entry), ==, APP_STATUS_INSTALLATION_SUCCESS);
  /* Don't check the store_app_status as it's updated asynchronously so that would racy */
}

/* Test Frome.DnldMgr.StartAppstoreDownload() */
static void
test_download_manager_start_appstore_download (Test *test,
                                               gconstpointer unused)
{
  install_bundle (test);

  remove_bundle (test);
}

/* Frome.DnldMgr.StartAppstoreDownload() failing because of a network
 * error with the download */
static void
test_download_manager_start_appstore_download_fail_network (Test *test,
                                                            gconstpointer unused)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;

  start_and_configure_frome (test);
  create_account_and_wait_account_status (test, "my@email.com");

  g_signal_connect (test->dl_mgr_proxy, "appstore-download-status",
                    G_CALLBACK (download_status_cb), test);

  frome_dnld_mgr_call_start_appstore_download (test->dl_mgr_proxy, APP_NAME,
                                               "http://fail.badger/test.bundle", PRODUCT_ID, APP_VERSION_1,
                                               FILE_SIZE, NULL, start_appstore_download_cb, test);

  test->wait = 2; /* start_appstore_download_cb and download_status_cb */
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert_true (test->result);

  g_assert_false (bundle_is_installed (NULL));

  g_assert_true (test->got_download_status_sig);
  g_assert_cmpstr (test->status_app_state, ==, "");
  g_assert_cmpstr (test->download_status, ==, FROME_APPSTORE_DOWNLOAD_STATUS_FAILED);
  g_assert_cmpint (test->download_status_result_code, ==, FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_DOWNLOADING_APP);

  entry = frome_app_db_find_entry (test->app_db, PRODUCT_ID, &test->error);
  g_assert_no_error (test->error);
  g_assert (entry != NULL);
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, PRODUCT_ID);
  g_assert_cmpuint (frome_app_db_entry_get_target_app_state (entry), ==, APP_STATE_DOWNLOAD_READY);
  g_assert_cmpuint (frome_app_db_entry_get_app_status (entry), ==, APP_STATUS_DOWNLOADING_FATAL_ERROR);
}

static void
cancel_appstore_download_cb (GObject *proxy,
                             GAsyncResult *result,
                             gpointer user_data)
{
  Test *test = user_data;

  test->result = frome_dnld_mgr_call_cancel_appstore_download_finish (FROME_DNLD_MGR (proxy),
                                                                      result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

/* Test Frome.DnldMgr.CancelAppstoreDownload() */
static void
test_download_manager_start_appstore_download_cancel (Test *test,
                                                      gconstpointer unused)
{
  g_autofree gchar *url = NULL;

  start_and_configure_frome (test);
  create_account_and_wait_account_status (test, "my@email.com");

  /* ensure the bundle isn't installed yet */
  remove_bundle (test);

  url = build_download_url (test, APP_VERSION_1);

  g_signal_connect (test->dl_mgr_proxy, "appstore-download-status",
                    G_CALLBACK (download_status_cb), test);

  frome_dnld_mgr_call_start_appstore_download (test->dl_mgr_proxy, APP_NAME,
                                               url, PRODUCT_ID, APP_VERSION_1,
                                               FILE_SIZE, NULL, start_appstore_download_cb, test);
  test->wait = 1;
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert_true (test->result);

  frome_dnld_mgr_call_cancel_appstore_download (test->dl_mgr_proxy, APP_NAME,
                                                url, PRODUCT_ID,
                                                NULL, cancel_appstore_download_cb, test);
  test->wait = 2; /* cancel_appstore_download_cb and download_status_cb */
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert_true (test->result);

  g_assert_true (test->got_download_status_sig);
  g_assert_cmpstr (test->status_app_state, ==, FROME_APPSTORE_SIG_STATE_LOADING);
  g_assert_cmpstr (test->download_status, ==, FROME_APPSTORE_DOWNLOAD_STATUS_CANCELLED);
  g_assert_cmpint (test->download_status_result_code, ==, FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_ERROR_DOWNLOAD_ABORTED_BY_USER);

  g_assert_false (bundle_is_installed (NULL));
}

static void
update_application_cb (GObject *proxy,
                       GAsyncResult *result,
                       gpointer user_data)
{
  Test *test = user_data;

  test->result = frome_dnld_mgr_call_update_application_finish (FROME_DNLD_MGR (proxy),
                                                                result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static void
update_bundle (Test *test)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;
  g_autofree gchar *url = NULL;
  InstallationStep step;
  g_autofree gchar *file_name = NULL;

  url = build_download_url (test, APP_VERSION_2);

  frome_dnld_mgr_call_update_application (test->dl_mgr_proxy, APP_NAME,
                                          url, PRODUCT_ID, APP_VERSION_2,
                                          FILE_SIZE, NULL,
                                          update_application_cb, test);

  test->wait = 1;
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert_true (test->result);

  entry = frome_app_db_find_entry (test->app_db, PRODUCT_ID, &test->error);
  g_assert_no_error (test->error);
  g_assert (entry != NULL);
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, PRODUCT_ID);
  g_assert_cmpstr (frome_app_db_entry_get_download_url (entry), ==, url);
  file_name = build_bundle_file_name (APP_VERSION_2);
  g_assert (g_str_has_suffix (frome_app_db_entry_get_install_path (entry), file_name));
  g_assert_cmpstr (frome_app_db_entry_get_app_name (entry), ==, APP_NAME);
  g_assert_cmpuint (frome_app_db_entry_get_target_app_state (entry), ==, APP_STATE_DOWNLOAD_READY);
  g_assert_cmpuint (frome_app_db_entry_get_store_app_state (entry), ==, APP_STATE_INSTALLING);
  g_assert_cmpuint (frome_app_db_entry_get_app_status (entry), ==, APP_STATUS_DOWNLOAD_STARTED);
  g_assert_cmpstr (frome_app_db_entry_get_version (entry), ==, APP_VERSION_2);
  g_assert_cmpuint (frome_app_db_entry_get_process_type (entry), ==, PROCESS_TYPE_APP_UPDATE);
  g_clear_object (&entry);

  step = STATE_WAIT_DOWNLOAD_PROGRESS_INSTALLING;
  while (step != STATE_DONE)
    {
      test->wait = 1;
      g_main_loop_run (test->loop);
      g_assert_no_error (test->error);
      g_assert_true (test->result);

      if (step == STATE_WAIT_DOWNLOAD_PROGRESS_INSTALLING)
        {
          g_assert_true (test->got_download_progress_sig);
          test->got_download_progress_sig = FALSE;

          if (g_strcmp0 (test->progress_app_state, FROME_APPSTORE_SIG_STATE_LOADING) == 0)
            /* We can receive a bunch of 'loading' signals informing us about download progress */
            continue;

          g_assert_cmpstr (test->progress_app_state, ==, FROME_APPSTORE_SIG_STATE_INSTALLING);
          /* App is installing wait for it to be installed */
          step = STATE_WAIT_DOWNLOAD_STATUS_INSTALLED;

          if (!test->got_download_status_sig)
            continue;

          /* We already received the next signal so no need to wait */
        }

      if (step == STATE_WAIT_DOWNLOAD_STATUS_INSTALLED)
        {
          g_assert_true (test->got_download_status_sig);
          test->got_download_status_sig = FALSE;

          g_assert_cmpstr (test->status_app_state, ==, FROME_APPSTORE_SIG_STATE_INSTALLED);
          g_assert_cmpstr (test->download_status, ==, FROME_APPSTORE_DOWNLOAD_STATUS_SUCCESS);
          g_assert_cmpint (test->download_status_result_code, ==, FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_NO_ERROR);

          step = STATE_DONE;
        }
      else
        g_assert_not_reached ();
    }

  entry = frome_app_db_find_entry (test->app_db, PRODUCT_ID, &test->error);
  g_assert_no_error (test->error);
  g_assert (entry != NULL);
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, PRODUCT_ID);
  g_assert_cmpuint (frome_app_db_entry_get_target_app_state (entry), ==, APP_STATE_INSTALLED);
  g_assert_cmpuint (frome_app_db_entry_get_app_status (entry), ==, APP_STATUS_INSTALLATION_SUCCESS);
  /* Don't check the store_app_status as it's updated asynchronously so that would racy */

  /* FIXME: check bundle is actually updated */
}

/* Test Frome.DnldMgr.UpdateApplication() */
static void
test_download_manager_update_application (Test *test,
                                          gconstpointer unused)
{
  install_bundle (test);
  update_bundle (test);
  remove_bundle (test);
}

/* Frome.DnldMgr.UpdateApplication() failing because the app hasn't been installed */
static void
test_download_manager_update_application_not_installed (Test *test,
                                                        gconstpointer unused)
{
  g_autofree gchar *url = NULL;

  start_and_configure_frome (test);
  create_account_and_wait_account_status (test, "my@email.com");

  url = build_download_url (test, APP_VERSION_1);

  frome_dnld_mgr_call_update_application (test->dl_mgr_proxy, APP_NAME,
                                          url, PRODUCT_ID, APP_VERSION_1,
                                          FILE_SIZE, NULL,
                                          update_application_cb, test);

  test->wait = 1;
  g_main_loop_run (test->loop);
  g_assert_error (test->error, FROME_DOWNLOAD_MANAGER_ERROR,
                  FROME_DOWNLOAD_MANAGER_ERROR_INVALID_ARGS);
  g_assert_false (test->result);
}

static void
uninstall_application_cb (GObject *proxy,
                          GAsyncResult *result,
                          gpointer user_data)
{
  Test *test = user_data;

  test->result = frome_dnld_mgr_call_uninstall_application_finish (FROME_DNLD_MGR (proxy),
                                                                   result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static void
uninstall_bundle (Test *test)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;

  frome_dnld_mgr_call_uninstall_application (test->dl_mgr_proxy, APP_NAME,
                                             PRODUCT_ID, NULL,
                                             uninstall_application_cb, test);

  test->wait = 2; /* uninstall_application_cb and download_status_cb */
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert_true (test->result);

  /* Uninstall is supposed to disable the application, not remove it from the file system */
  g_assert_true (bundle_is_installed (APP_VERSION_1));

  g_assert_true (test->got_download_status_sig);
  g_assert_cmpstr (test->status_app_state, ==, FROME_APPSTORE_SIG_STATE_UNINSTALLED);
  g_assert_cmpstr (test->download_status, ==, FROME_APPSTORE_DOWNLOAD_STATUS_SUCCESS);
  g_assert_cmpint (test->download_status_result_code, ==, FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_NO_ERROR);

  entry = frome_app_db_find_entry (test->app_db, PRODUCT_ID, &test->error);
  g_assert_no_error (test->error);
  g_assert (entry != NULL);
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, PRODUCT_ID);
  g_assert_cmpuint (frome_app_db_entry_get_target_app_state (entry), ==, APP_STATE_UNINSTALLED);
  g_assert_cmpuint (frome_app_db_entry_get_process_type (entry), ==, PROCESS_TYPE_APP_UNINSTALL);
}

/* Test Frome.DnldMgr.UninstallApplication() */
static void
test_download_manager_uninstall_application (Test *test,
                                             gconstpointer unused)
{
  install_bundle (test);
  uninstall_bundle (test);

  remove_bundle (test);
}

/* Test Frome.DnldMgr.UninstallApplication() */
static void
test_download_manager_uninstall_application_not_installed (Test *test,
                                                           gconstpointer unused)
{
  start_and_configure_frome (test);
  create_account_and_wait_account_status (test, "my@email.com");

  frome_dnld_mgr_call_uninstall_application (test->dl_mgr_proxy, APP_NAME,
                                             PRODUCT_ID, NULL,
                                             uninstall_application_cb, test);

  test->wait = 1;
  g_main_loop_run (test->loop);
  g_assert_error (test->error, FROME_DOWNLOAD_MANAGER_ERROR,
                  FROME_DOWNLOAD_MANAGER_ERROR_INVALID_ARGS);
  g_assert_false (test->result);
}

static void
reinstall_application_cb (GObject *proxy,
                          GAsyncResult *result,
                          gpointer user_data)
{
  Test *test = user_data;

  test->result = frome_dnld_mgr_call_reinstall_application_finish (FROME_DNLD_MGR (proxy),
                                                                   result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

/* Test Frome.DnldMgr.ReinstallApplication() */
static void
test_download_manager_reinstall_application (Test *test,
                                             gconstpointer unused)
{
  g_autoptr (FromeAppDBEntry) entry = NULL;

  install_bundle (test);
  uninstall_bundle (test);

  frome_dnld_mgr_call_reinstall_application (test->dl_mgr_proxy, APP_NAME,
                                             PRODUCT_ID, NULL,
                                             reinstall_application_cb, test);

  test->wait = 2; /* reinstall_application_cb and download_status_cb */
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert_true (test->result);

  g_assert_true (bundle_is_installed (APP_VERSION_1));

  g_assert_true (test->got_download_status_sig);
  g_assert_cmpstr (test->status_app_state, ==, FROME_APPSTORE_SIG_STATE_REINSTALL);
  g_assert_cmpstr (test->download_status, ==, FROME_APPSTORE_DOWNLOAD_STATUS_SUCCESS);
  g_assert_cmpint (test->download_status_result_code, ==, FROME_APPSTORE_DOWNLOAD_STATUS_RESULT_NO_ERROR);

  entry = frome_app_db_find_entry (test->app_db, PRODUCT_ID, &test->error);
  g_assert_no_error (test->error);
  g_assert (entry != NULL);
  g_assert_cmpstr (frome_app_db_entry_get_product_id (entry), ==, PRODUCT_ID);
  g_assert_cmpuint (frome_app_db_entry_get_target_app_state (entry), ==, APP_STATE_INSTALLED);
  g_assert_cmpuint (frome_app_db_entry_get_process_type (entry), ==, PROCESS_TYPE_APP_REINSTALL);

  remove_bundle (test);
}

/* Frome.DnldMgr.ReinstallApplication() failing because the app hasn't been installed */
static void
test_download_manager_reinstall_application_not_installed (Test *test,
                                                           gconstpointer unused)
{
  start_and_configure_frome (test);
  create_account_and_wait_account_status (test, "my@email.com");

  frome_dnld_mgr_call_reinstall_application (test->dl_mgr_proxy, APP_NAME,
                                             PRODUCT_ID, NULL,
                                             reinstall_application_cb, test);

  test->wait = 1; /* reinstall_application_cb and download_status_cb */
  g_main_loop_run (test->loop);
  g_assert_error (test->error, FROME_DOWNLOAD_MANAGER_ERROR,
                  FROME_DOWNLOAD_MANAGER_ERROR_INVALID_ARGS);
  g_assert_false (test->result);
}

static void
rollback_application_cb (GObject *proxy,
                         GAsyncResult *result,
                         gpointer user_data)
{
  Test *test = user_data;

  test->result = frome_dnld_mgr_call_rollback_application_finish (FROME_DNLD_MGR (proxy),
                                                                  result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

/* Test Frome.DnldMgr.RollbackApplication() */
static void
test_download_manager_rollback_application (Test *test,
                                            gconstpointer unused)
{
  install_bundle (test);
  update_bundle (test);

  frome_dnld_mgr_call_rollback_application (test->dl_mgr_proxy, APP_NAME,
                                            PRODUCT_ID, NULL,
                                            rollback_application_cb, test);

  test->wait = 1;
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert_true (test->result);

  /* Can't check if the DB has been updated as Frome doesn't fire any signal
   * when the rollback is finished */

  /* Can't remove the bundle because of T3233. It will be removed when starting
   * the next test. */
}

int
main (int argc, char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/frome/service/activation/full-name", Test, NULL,
              setup, test_activation_full_name, teardown);
  g_test_add ("/frome/service/activation/account-manager", Test, NULL,
              setup, test_activation_account_manager, teardown);
  g_test_add ("/frome/service/activation/download-manager", Test, NULL,
              setup, test_activation_download_manager, teardown);

  g_test_add ("/frome/service/account-manager/create-account/success", Test, NULL,
              setup, test_account_manager_create_account, teardown);
  g_test_add ("/frome/service/account-manager/create-account/call-fail", Test, NULL,
              setup, test_account_manager_create_account_call_fail, teardown);
  g_test_add ("/frome/service/account-manager/create-account/sig-fail", Test, NULL,
              setup, test_account_manager_create_account_sig_fail, teardown);
  g_test_add ("/frome/service/account-manager/delete-account/success", Test, NULL,
              setup, test_account_manager_delete_account, teardown);
  g_test_add ("/frome/service/account-manager/delete-account/fail", Test, NULL,
              setup, test_account_manager_delete_account_fail, teardown);
  g_test_add ("/frome/service/account-manager/get-accounts/success", Test, NULL,
              setup, test_account_manager_get_accounts, teardown);
  g_test_add ("/frome/service/account-manager/set-default-account/success", Test, NULL,
              setup, test_account_manager_set_default_account, teardown);
  g_test_add ("/frome/service/account-manager/set-default-account/fail", Test, NULL,
              setup, test_account_manager_set_default_account_fail, teardown);
  g_test_add ("/frome/service/account-manager/set-account-type/success", Test, NULL,
              setup, test_account_manager_set_account_type, teardown);
  g_test_add ("/frome/service/account-manager/set-account-type/fail", Test, NULL,
              setup, test_account_manager_set_account_type_fail, teardown);

  g_test_add ("/frome/service/download-manager/start-appstore-download/success", Test, NULL,
              setup, test_download_manager_start_appstore_download, teardown);
  g_test_add ("/frome/service/download-manager/start-appstore-download/fail/network", Test, NULL,
              setup, test_download_manager_start_appstore_download_fail_network, teardown);
  g_test_add ("/frome/service/download-manager/start-appstore-download/cancel", Test, NULL,
              setup, test_download_manager_start_appstore_download_cancel, teardown);
  g_test_add ("/frome/service/download-manager/update-application/success", Test, NULL,
              setup, test_download_manager_update_application, teardown);
  g_test_add ("/frome/service/download-manager/update-application/not-installed", Test, NULL,
              setup, test_download_manager_update_application_not_installed, teardown);
  g_test_add ("/frome/service/download-manager/uninstall-application/success", Test, NULL,
              setup, test_download_manager_uninstall_application, teardown);
  g_test_add ("/frome/service/download-manager/uninstall-application/not-installed", Test, NULL,
              setup, test_download_manager_uninstall_application_not_installed, teardown);
  g_test_add ("/frome/service/download-manager/reinstall-application/success", Test, NULL,
              setup, test_download_manager_reinstall_application, teardown);
  g_test_add ("/frome/service/download-manager/reinstall-application/not-installed", Test, NULL,
              setup, test_download_manager_reinstall_application_not_installed, teardown);
  g_test_add ("/frome/service/download-manager/rollback-application/success", Test, NULL,
              setup, test_download_manager_rollback_application, teardown);

  return g_test_run ();
}
