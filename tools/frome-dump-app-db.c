/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>

#include "app-db.h"

static void
dump_entry (FromeAppDBEntry *entry)
{
  g_print ("Product %s\n", frome_app_db_entry_get_product_id (entry));
  g_print ("  URL: %s\n", frome_app_db_entry_get_download_url (entry));
  g_print ("  Install path: %s\n", frome_app_db_entry_get_install_path (entry));
  g_print ("  App folder name: %s\n", frome_app_db_entry_get_app_folder_name (entry));
  g_print ("  App name: %s\n", frome_app_db_entry_get_app_name (entry));
  g_print ("  Target app state: %s\n", app_state_to_str (frome_app_db_entry_get_target_app_state (entry)));
  g_print ("  Store app state: %s\n", app_state_to_str (frome_app_db_entry_get_store_app_state (entry)));
  g_print ("  App status: %s\n", app_status_to_str (frome_app_db_entry_get_app_status (entry)));
  g_print ("  App schemas: %s\n", frome_app_db_entry_get_app_schemas (entry));
  g_print ("  Config file: %s\n", frome_app_db_entry_get_config_file (entry));
  g_print ("  Version: %s\n", frome_app_db_entry_get_version (entry));
  g_print ("  App users: %s\n", frome_app_db_entry_get_app_users (entry));
  g_print ("  Rollback version: %s\n", frome_app_db_entry_get_rollback_version (entry));
  g_print ("  Process type: %s\n", process_type_to_str (frome_app_db_entry_get_process_type (entry)));
  g_print ("  Update prod ID: %s\n", frome_app_db_entry_get_update_prod_id (entry));
  g_print ("\n");
}

static void
dump_db (FromeAppDB *db)
{
  g_autoptr (GPtrArray) items = NULL;
  g_autoptr (GError) error = NULL;
  guint i;

  items = frome_app_db_get_all_entries (db, &error);
  if (items == NULL)
    {
      g_warning ("Failed to retrieve items: %s", error->message);
      return;
    }

  for (i = 0; i < items->len; i++)
    {
      FromeAppDBEntry *entry = g_ptr_array_index (items, i);

      dump_entry (entry);
    }
}

int
main (int argc,
      char *argv[])
{
  g_autoptr (GError) error = NULL;
  g_autofree gchar *file = NULL;
  g_autoptr (FromeAppDB) db = NULL;
  GOptionContext *context;

  const GOptionEntry entries[] =
      {
        { "file", 'f', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &file, "DB file", "a" },
        {
            NULL,
        },
      };

  context = g_option_context_new ("- Frome Service");
  g_option_context_add_main_entries (context, entries, NULL);
  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_print ("Option parsing failed: %s\n", error->message);
      return -1;
    }

  if (file == NULL)
    file = g_build_filename (g_get_user_data_dir (), "frome", "applications.db", NULL);

  db = frome_app_db_new (file);
  dump_db (db);

  return 0;
}
